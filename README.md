# TR181 compatible MQTT client

## Introduction

This is an Ambiorix plug-in for a TR-181 compatible MQTT client.

## Building

### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [libmosquitto](https://gitlab.com/soft.at.home/network/libmosquitto)
- [libc-ares](https://gitlab.com/soft.at.home/network/libc-ares)
- [libnetmodel](https://gitlab.com/prpl-foundation/components/core/libraries/libnetmodel)

You can install these libraries from source or using their debian packages. To install them from source, refer to their corresponding repositories for more information.
To install them using debian packages, you can run

```bash
sudo apt update
sudo apt install sah-lib-sahtrace-dev libamxc libamxp libamxj libamxd libamxb libamxo libimtp sah-lib-mosquitto-dev libnetmodel libc-ares-dev
```

Note that a patched version of `mosquitto` is needed, because this contains 2 extra API functions for asynchronously connecting to the MQTT broker, which are not available in the open source version [yet](https://github.com/eclipse/mosquitto/pull/2345).

### Build and install tr181-mqtt

1. Clone the git repository

    To be able to build it, you need the source code. So open the desired target directory and clone this plug-in in it.

    ```bash
    mkdir ~/workspace/amx/plugins
    cd ~/workspace/amx/plugins
    git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-mqtt.git
    ``` 

1. Build it

    When using the internal gitlab, you must define an environment variable `VERSION_PREFIX` before building.

    ```bash
    export VERSION_PREFIX="master_"
    ```

    After the variable is set, you can build the plug-in.

    ```bash
    cd ~/workspace/amx/plugins/tr181-mqtt
    make
    ```

1. Install it

    You can use the install target in the makefile to install the plug-in

    ```bash
    cd ~/workspace/amx/plugins/tr181-mqtt
    sudo -E make install
    ```

### Running the plug-in

During installation a symbolic link is created to amxrt:

```text
/usr/bin/tr181-mqtt -> /usr/bin/amxrt
```

This allows you to run the MQTT client using the `tr181-mqtt` command. `amxrt` will find the relevant odl files in `/etc/amx/tr181-mqtt`. In the current configuration (see `odl/tr181-mqtt.odl`) all files from the directory `/etc/amx/tr181-mqtt/defaults` are loaded on startup of the MQTT plug-in. You can add your own odl files here if you want to add your own `MQTT.Client.` instances.

### Testing

#### Run tests

1. Install dependencies

    No extra dependencies are needed for testing the `tr181-mqtt`.

1. Run tests

    You can run the tests by executing the following command.
    
    ```bash
    cd ~/workspace/amx/plugins/tr181-mqtt/test
    make
    ```
    
    Or this command if you also want the coverage tests to run:
    
    ```bash
    cd ~/workspace/amx/plugins/tr181-mqtt/test
    make run coverage
    ```
    
    Or from the root directory of this repository,
    
    ```bash
    cd ~/workspace/amx/plugins/tr181-mqtt
    make test
    ```
    
    This last will run the unit-tests and generate the test coverage reports in one go.

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c file) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/workspace/amx/plugins/tr181-mqtt/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/workspace/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/amx/plugins/tr181-mqtt/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
USER@<CID>:~/amx/plugins/tr181-mqtt/$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad 
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link 
       valid_lft forever preferred_lft forever
```

In this case the IP address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/amx/plugins/tr181-mqtt/output/x86_64-linux-gnu/coverage/report/`
