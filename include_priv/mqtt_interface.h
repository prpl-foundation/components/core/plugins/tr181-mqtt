/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MQTT_INTERFACE_H__)
#define __MQTT_INTERFACE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <ares.h>
#include <imtp/imtp_connection.h>

#include "mqtt.h"
#include "netmodel/client.h"

/******************************************************************************
   START OF HACK
   The following structs should not be defined here.
   They are copies from the defined structs in libmosquitto

   The reason why it is copied from property_mosq.h:
   - 1. No public interface to iterator over all properties
   - 2. The structure is in property_mosq.h and is not installed
 */
struct mqtt__string {
    char* v;
    int len;
};

struct mqtt5__property {
    struct mqtt5__property* next;
    union {
        uint8_t i8;
        uint16_t i16;
        uint32_t i32;
        uint32_t varint;
        struct mqtt__string bin;
        struct mqtt__string s;
    } value;
    struct mqtt__string name;
    int32_t identifier;
    bool client_generated;
};
/******************************************************************************/

typedef enum _mqtt_sub_status {
    mqtt_sub_status_unsubscribed,
    mqtt_sub_status_subscribed,
    mqtt_sub_status_subscribing,
    mqtt_sub_status_unsubscribing,
    mqtt_sub_status_error
} mqtt_sub_status_t;

typedef struct _mqtt_sub_t {
    amxc_llist_it_t it;
    char* topic;
    mqtt_sub_status_t status;
    int32_t message_id;
    amxd_object_t* sub_obj;
} mqtt_sub_t;

/* Created during mqtt_new i.e. when a client connects to a broker */
typedef struct _mqtt_con_t {
    amxc_llist_it_t it;             // iterator to find container in a linked list
    amxc_llist_t items;             // linked list of mqtt_item_t
    amxc_llist_t subs;              // linked list of mqtt_sub_t
    void* client_inst;              // ptr to an amxd_object_t i.e. the object of interest in the datamodel
    void* lib_data;                 // ptr to data returned by mosquitto_new
    int mqtt_fd;                    // fd to add to event loop for mqtt messages
    amxp_timer_t* misc_timer;       // timer to call mosquitto_loop_misc
} mqtt_con_t;

typedef struct _mqtt_stats_t {
    uint64_t publish_sent;
    uint64_t publish_recv;
    uint64_t subscribe_sent;
    uint64_t unsubscribe_sent;
    uint64_t mqtt_msg_sent;
    uint64_t mqtt_msg_recv;
    uint32_t con_errors;
    uint32_t pub_errors;
} mqtt_stats_t;

typedef struct _mqtt_client_interface_t {
    char* name;
    bool isup;
    netmodel_query_t* query_isup;
} mqtt_client_interface_t;

typedef struct _dns_query {
    ares_channel channel;
    struct ares_options options;
    int optmask;
    int socket_fd;
    amxp_timer_t* retransmission;
    amxc_var_t intervals;
    amxc_var_t* interval;
} dns_query_t;

/* Created when a client connects to a broker through the mqtt client */
typedef struct _mqtt_client {
    mqtt_con_t* con;
    mqtt_stats_t stats;
    amxc_llist_t imtp_cons;
    uint64_t reconnect_id;          // call id to track ForceReconnect
    bool reconnect_busy;            // boolean to indicate ForceReconnect is in progress
    amxp_timer_t* con_retry_timer;  // timer to track when a reconnect should be tried
    uint32_t nbr_con_retries;
    mqtt_client_interface_t interface;
    dns_query_t dns_query;          // Used to keep track of things related to the dns lookup of the BrokerAddress
    bool needs_clean_start;         // Used to check if cleaning the connection is needed
} mqtt_client_t;

typedef struct _mqtt_imtp {
    amxc_llist_it_t it;                 // linked list iterator to track several of these in mqtt_client_t
    imtp_connection_t* listen_con;      // connection for the listen socket
    imtp_connection_t* accepted_con;    // accepted connection
    mqtt_client_t* client;              // pointer to client 'parent' struct
    char* topics;                       // topic that service is interested in
} mqtt_imtp_t;

typedef struct _mqtt_item {
    amxc_lqueue_it_t qit;
    amxc_llist_it_t lit;
    amxc_var_t data;
    void* payload;
    int payloadlen;
} mqtt_item_t;

typedef enum _mqtt_item_types {
    mqtt_item_invalid,
    mqtt_item_connect,
    mqtt_item_disconnect,
    mqtt_item_message,
    mqtt_item_publish,
    mqtt_item_subscribe,
    mqtt_item_unsubscribe,
    mqtt_item_max
} mqtt_item_type_t;

typedef void (* mqtt_handler_t) (mqtt_con_t* con, mqtt_item_t* item);

void PRIVATE mqtt_init_lib(void);
void PRIVATE mqtt_cleanup_lib(void);
int PRIVATE mqtt_new(mqtt_con_t** con,
                     const char* client_id,
                     bool clean_session,
                     const char* mqtt_version,
                     void* priv);
void PRIVATE mqtt_delete(mqtt_con_t** con);

void mqtt_client_interface_clean(amxd_object_t* object);
int PRIVATE mqtt_tls_setup(mqtt_con_t* con, amxc_var_t* params);
void PRIVATE mqtt_finish_connect(int fd, void* priv);
int PRIVATE mqtt_connect(mqtt_con_t* con,
                         const char* host,
                         int port,
                         int keepalive,
                         const char* username,
                         const char* pwd,
                         const char* proto_version,
                         uint32_t tcp_send_mem,
                         amxc_var_t* user_props);
int PRIVATE mqtt_disconnect(mqtt_con_t* con, int reason);
int PRIVATE mqtt_subscribe(mqtt_con_t* con,
                           int* message_id,
                           const char* subscribe_pattern,
                           int qos,
                           int options);
int PRIVATE mqtt_unsubscribe(mqtt_con_t* con,
                             int* message_id,
                             const char* pattern);

int PRIVATE mqtt_publish(mqtt_con_t* con,
                         int* message_id,
                         const char* topic,
                         int payloadlen,
                         const void* payload,
                         int qos,
                         bool retain,
                         amxc_var_t* mqtt_props);

int PRIVATE mqtt_topic_matches_sub(const char* sub,
                                   const char* topic,
                                   bool* result);

int PRIVATE mqtt_ctrl_init(void);
void PRIVATE mqtt_ctrl_clean(void);
int PRIVATE mqtt_ctrl_write(mqtt_con_t* con,
                            mqtt_item_t* item);
int mqtt_ctrl_read(mqtt_con_t** con,
                   mqtt_item_t** item);

int PRIVATE mqtt_ctrl_fd(void);
int PRIVATE mqtt_ctrl_lock(void);
int PRIVATE mqtt_ctrl_unlock(void);

void PRIVATE mqtt_ctrl_set_handler(mqtt_item_type_t id, mqtt_handler_t fn);
void PRIVATE mqtt_ctrl_handle_item(int fd, void* priv);

#ifdef __cplusplus
}
#endif

#endif // __MQTT_INTERFACE_H__
