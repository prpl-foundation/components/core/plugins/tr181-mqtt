# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.6.5 - 2024-12-20(07:57:56 +0000)

### Other

- Device.MQTT.Client.{i}.Password is readable

## Release v3.6.4 - 2024-12-05(10:13:20 +0000)

### Other

- - [MQTT] MQTT Client Instance gets removed automatically when trying to set BrokerAddress

## Release v3.6.3 - 2024-10-08(09:01:59 +0000)

### Other

- TR181-MQTT - Crash after several failed connections attempts

## Release v3.6.2 - 2024-10-03(13:33:33 +0000)

### Other

- [Crash] tr181-mqtt crash

## Release v3.6.1 - 2024-09-30(05:51:36 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v3.6.0 - 2024-09-13(09:14:45 +0000)

### Other

- [SAHPairing] It must be possible to select the topics you want to receive messages from

## Release v3.5.6 - 2024-07-29(08:22:03 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v3.5.5 - 2024-07-16(11:42:29 +0000)

### Other

- TR-181 Device.MQTT data model issues 19.03.2024

## Release v3.5.4 - 2024-07-02(09:46:46 +0000)

### Other

- [SAHPairing]: crash of tr181-mqtt blocks sahpairing

## Release v3.5.3 - 2024-07-01(10:04:57 +0000)

### Other

- [TR181-MQTT] Add tr181-mqtt to ProcessMonitor

## Release v3.5.2 - 2024-06-27(14:06:10 +0000)

### Other

- [SAHPairing]: crash of tr181-mqtt blocks sahpairing

## Release v3.5.1 - 2024-05-08(11:13:26 +0000)

### Other

- Revert usage of separate defaults dir for HGW and APs

## Release v3.5.0 - 2024-05-07(16:03:27 +0000)

### New

- Send handshake on IMTP connection

## Release v3.4.5 - 2024-04-12(13:13:36 +0000)

### Other

- [MQTT Client] Update Stats when doing a publish() DM API call.

## Release v3.4.4 - 2024-04-11(07:55:48 +0000)

### Fixes

- [MQTT] Publish can fail, but reason is not shown

## Release v3.4.3 - 2024-04-10(07:09:53 +0000)

### Changes

- Make amxb timeouts configurable

## Release v3.4.2 - 2024-03-21(09:22:39 +0000)

### Other

- [AP Config] AP and HGW are not paired after upgrade or reboot

## Release v3.4.1 - 2024-02-09(12:04:55 +0000)

### Other

- [AP Config] AP and HGW are not paired after the upgrade

## Release v3.4.0 - 2024-02-05(16:18:36 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v3.3.2 - 2024-01-24(09:21:47 +0000)

### Fixes

- Validate the reason a validation function is called

## Release v3.3.1 - 2024-01-22(07:33:31 +0000)

### Other

- [Events Log][System] Device Login

## Release v3.3.0 - 2024-01-17(09:30:56 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v3.2.4 - 2023-12-15(09:29:44 +0000)

### Other

- [KPN][AP] tr181-mqtt -D crash

## Release v3.2.3 - 2023-12-08(14:17:51 +0000)

### Other

- [MQTT] Remove default client from odl file

## Release v3.2.2 - 2023-11-28(11:23:37 +0000)

### Other

- Move component to prpl-foundation gitlab

## Release v3.2.1 - 2023-11-09(09:28:45 +0000)

### Other

- Refactor libamxo - libamxp: move fd and connection management out of libamxo

## Release v3.2.0 - 2023-11-07(10:51:24 +0000)

### Changes

- [MQTT] Remove dependency on USP backend

## Release v3.1.4 - 2023-10-13(14:14:04 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v3.1.3 - 2023-09-29(14:08:59 +0000)

### Other

- Reenable persistency for tr181-mqtt

## Release v3.1.2 - 2023-09-19(15:10:56 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v3.1.1 - 2023-08-18(10:22:15 +0000)

### Fixes

- [USP] The board fails to connect to the MQTT broker in TCP/IP

## Release v3.1.0 - 2023-08-17(10:23:47 +0000)

### Other

- Add SSL Engine support to TR181-MQTT

## Release v3.0.2 - 2023-08-16(14:39:45 +0000)

### Other

- lib amx mqtt client only receives messages from tr181-mqtt with topic set to NULL

## Release v3.0.1 - 2023-08-03(08:55:22 +0000)

### Fixes

- [USP] Activation is failing during onboarding state - Onboarding is not working after a Factory Reset

## Release v3.0.0 - 2023-07-11(11:27:42 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v2.12.13 - 2023-07-06(08:06:42 +0000)

### Fixes

- [REGRESSION][USP][CDROUTER] usp-endpoint-id missing in the MQTT CONNECT packet

## Release v2.12.12 - 2023-07-03(13:27:05 +0000)

### Fixes

- Init script has no shutdown function

## Release v2.12.11 - 2023-06-27(07:50:13 +0000)

### Other

- [CR9HF] - Multiple tr181-mqtt sessions running

## Release v2.12.10 - 2023-06-23(08:27:21 +0000)

### Fixes

- [V12][MQTT]: Very low frequency of Retry Mechanism

## Release v2.12.9 - 2023-06-15(08:39:20 +0000)

### Other

- USP MQTT Client cannot connect to USP Broker

## Release v2.12.8 - 2023-05-26(07:24:01 +0000)

### Fixes

- [USP][MQTT] Missing unique keys for MQTT data model

## Release v2.12.7 - 2023-05-08(07:15:25 +0000)

### Fixes

- [MQTT] Client Status no longer goes to Error

## Release v2.12.6 - 2023-04-28(11:45:15 +0000)

### Fixes

- [USP][DNS] Resolution is not triggered systematically when Wan was NOK and recover

## Release v2.12.5 - 2023-04-21(10:32:31 +0000)

### Fixes

- [V12][23.04.12-D] RG onboarding behaves differently on VDSL lines and on FIBER (possible DNS resolution timing issue on VDSL?)

## Release v2.12.4 - 2023-04-13(12:03:50 +0000)

### Fixes

- Make sure BrokerAddress is used in connect

## Release v2.12.3 - 2023-04-05(14:16:02 +0000)

### Fixes

- Must use hostname in connect for certificate checking

## Release v2.12.2 - 2023-04-05(10:43:50 +0000)

### Changes

- [MQTT][USP] Make client auto reconnect configurable

## Release v2.12.1 - 2023-04-03(08:43:31 +0000)

### Fixes

- [ACS] [V12]: No Request sent from Device side and Inconsistent onboarding behaviour of V12 on MQTT server

## Release v2.12.0 - 2023-03-23(15:23:05 +0000)

### Other

- Use sah_libc-ares instead of opensource_c-ares

## Release v2.11.1 - 2023-03-20(11:28:58 +0000)

### Fixes

- [MQTT] DNS resolving can still block

## Release v2.11.0 - 2023-03-15(11:29:22 +0000)

### Other

- Exposure of Device.MQTT.Client. instances for both Uber/BDD/Lansec and USP

## Release v2.10.4 - 2023-03-09(09:19:15 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v2.10.3 - 2023-02-28(07:11:31 +0000)

### Fixes

- [V12] [USP] GET Message Object = Device. - Retry mechanism is not working

## Release v2.10.2 - 2023-02-24(11:42:13 +0000)

### Other

- [MQTT] Remove protected attribute from Interface parameter

## Release v2.10.1 - 2023-02-23(09:25:11 +0000)

### Changes

- [USP] Redirection to Management Broker without the need of ForceReconnect() command

## Release v2.10.0 - 2023-02-21(15:45:00 +0000)

### Other

- [PRPL] Make MSS work on prpl config

## Release v2.9.5 - 2023-02-16(10:29:44 +0000)

### Other

- Add tr181-mqtt/tr181-localagent/uspagent into processmonitor
- Setup direct pcb connection for tr181-localagent

## Release v2.9.4 - 2023-02-07(08:36:15 +0000)

### Other

- [SAHPairing] Make sahpairing work with MQTT client

## Release v2.9.3 - 2023-01-31(08:15:12 +0000)

### Fixes

- [MQTT][USP] Overlapping reconnects can cause a segmentation fault

## Release v2.9.2 - 2023-01-06(13:04:21 +0000)

### Other

- [KPN SW2][Security]Restrict ACL of admin user

## Release v2.9.1 - 2023-01-02(07:39:56 +0000)

### Fixes

- [TR181-MQTT Client] tr181-mqtt wants a non empty ClientID with MQTT 3.1.1

## Release v2.9.0 - 2022-12-13(09:04:47 +0000)

### Other

- Add interop with different MQTT Broker version

## Release v2.8.1 - 2022-12-09(09:29:21 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v2.8.0 - 2022-11-16(08:03:57 +0000)

### New

- Add TcpSendMem parameter to Client object

## Release v2.7.3 - 2022-10-26(06:54:37 +0000)

### Changes

- It must be possible to accept multiple MQTT connections per Client

## Release v2.7.2 - 2022-10-24(10:00:12 +0000)

### Fixes

- [MQTT] Segmentation fault in tr181-mqtt after enabling client

## Release v2.7.1 - 2022-09-20(11:52:37 +0000)

### Other

- Disable opensource CI
- Use sah-lib-mosquitto-dev in debian package dependencies

## Release v2.7.0 - 2022-09-15(12:53:27 +0000)

### Other

- [BART] Create ambiorix bart module

## Release v2.6.0 - 2022-09-09(15:25:57 +0000)

### Other

- [USP] Open source tr181-mqtt
- [BART] Create ambiorix bart module

## Release v2.5.3 - 2022-07-20(08:39:07 +0000)

### Fixes

- Certificate and config to connect to FUT backend

## Release v2.5.2 - 2022-07-18(07:39:19 +0000)

### Other

- [KPN][ROB] CPU increased has been observed

## Release v2.5.1 - 2022-07-05(07:28:08 +0000)

### Fixes

- [MQTT] TLS parameters should be persistent

## Release v2.5.0 - 2022-06-20(15:08:36 +0000)

### Other

- Support TLS for MQTT.Client instance used for USP

## Release v2.4.7 - 2022-06-09(08:10:42 +0000)

### Other

- [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

## Release v2.4.6 - 2022-05-19(12:47:00 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v2.4.5 - 2022-05-05(12:55:55 +0000)

### Fixes

- [MQTT] Topic must be writable after creation

## Release v2.4.4 - 2022-04-29(14:40:33 +0000)

### Fixes

- [MQTT] Clients connecting with the same ID cause conflicts

## Release v2.4.3 - 2022-04-21(08:04:17 +0000)

### Fixes

- [AMX] Hidden parameter cannot be saved correctly

## Release v2.4.2 - 2022-04-15(06:36:14 +0000)

### Fixes

- amxp timer works with miliseconds

## Release v2.4.1 - 2022-04-13(09:46:33 +0000)

### Fixes

- [Buildsystem] Pipes are stripped from odl files when they are installed [fix]

## Release v2.4.0 - 2022-04-12(16:16:16 +0000)

### New

- [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

## Release v2.3.9 - 2022-03-28(09:11:08 +0000)

### Changes

- [USP] Re-enable data model persistency in uspagent and tr181-mqtt

## Release v2.3.8 - 2022-03-24(14:20:56 +0000)

### Fixes

- [MQTT] Legacy code needs to be removed

## Release v2.3.7 - 2022-03-24(10:45:52 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v2.3.6 - 2022-03-21(12:52:41 +0000)

### Fixes

- Set KeepAliveTime to 0 by default [revert]

## Release v2.3.5 - 2022-03-18(10:05:30 +0000)

### Fixes

- Doc generation for tr181-mqtt component (amx)

## Release v2.3.4 - 2022-03-14(14:20:54 +0000)

### Fixes

- Set KeepAliveTime to 0 by default

## Release v2.3.3 - 2022-02-25(11:47:17 +0000)

### Other

- Enable core dumps by default
- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v2.3.2 - 2022-01-28(14:20:40 +0000)

### Fixes

- [IMTP] Need to reread large IMTP messages

## Release v2.3.1 - 2022-01-06(08:31:10 +0000)

### Fixes

- [MQTT] Properties should only be added for MQTT 5.0

## Release v2.3.0 - 2022-01-04(16:35:10 +0000)

### New

- tr181-mqtt has trouble connecting due to DNS issue at boot

## Release v2.2.6 - 2021-12-17(13:13:39 +0000)

### Fixes

- [MQTT] tr181-mqtt should not try to extract msg id from TLV

## Release v2.2.5 - 2021-12-13(11:58:03 +0000)

### Changes

- [USP] Agent must publish responses on controller's reply-to topic

## Release v2.2.4 - 2021-10-11(14:51:24 +0000)

### Fixes

- Segmentation fault when disconnecting client

## Release v2.2.3 - 2021-10-08(11:59:49 +0000)

### Fixes

- [ACL] Add default ACL configuration per services
- [USP] MQTT clients should send ping messages to stay connected

### Other

- Add example ACL files

## Release v2.2.2 - 2021-09-16(07:19:05 +0000)

### Fixes

- Issue #27: Remove references to libmosquitto_poc from README
- [USP] The USP Agent and MQTT client should have the USP backend as run time dependency

## Release v2.2.1 - 2021-09-09(12:05:38 +0000)

### Fixes

- [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt

## Release v2.2.0 - 2021-08-31(06:09:52 +0000)

### New

- Extend logging for MQTT properties

## Release v2.1.3 - 2021-08-27(06:33:34 +0000)

### Fixes

- tr181-mqtt should use tagged version of libmosquitto

## Release v2.1.2 - 2021-08-25(13:19:45 +0000)

### Fixes

- Remove socket fd from event loop before disconnecting the client

## Release v2.1.1 - 2021-08-24(12:03:18 +0000)

### Other

- USP components should have debian packages

## Release v2.1.0 - 2021-08-23(15:12:40 +0000)

### New

- [USP][CDROUTER] USP Agent should provide a reply-to topic in MQTT messages

## Release v2.0.1 - 2021-08-20(09:52:44 +0000)

### Fixes

- Missing linker flag for libamxj

## Release v2.0.0 - 2021-08-19(06:49:24 +0000)

### Breaking

- Use internal mosquitto library

## Release v1.4.0 - 2021-07-19(13:39:05 +0000)

### New

- [USP][CDROUTER] USP MQTT Publish messages includes invalid Content Type property (empty)

## Release v1.3.2 - 2021-07-12(12:22:51 +0000)

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

## Release v1.3.0 - 2021-05-04(07:06:49 +0000)

### New

- The MQTT client datamodel must be extended with user property support 

## Release 1.2.2 - 2021-04-27(15:37:19 +0000)

### Added

- Implement the Device.MQTT.Client.{i}.ForceReconnect() function

### Changed

- Removes debug prints

### Fixed

- Close connections on exit

## Release 1.2.1 - 2021-04-13(09:07:07 +0000)

### Changed

-  MQTT Client settings must be boot persistent [change]

### Fixed

- Built-in data model RPC methods can conflict with domain specific RPCs [fix]

## Release 1.2.0 - 2021-04-12(18:58:59 +0200)

### Added

- Add README.md

### Changed

- The tr181-mqtt must be startable with or without eventing enabled during load [fix]
- CreateListenSocket should fail if there already is an active connection

## Release 1.1.2 - 2021-03-17(10:02:37 +0000)

### Changed

- Allow incoming TLVs without message ID

## Release 1.1.1 - 2021-03-12(14:51:07 +0000)

### Changed

- Let amxrt dynamically load backends from /usr/bin/mods/amxb/ if they are available

## Release 1.1.0 - 2021-03-09(14:51:09 +0000)

### Added

- Add object-defined method to create a backend listen socket

### Changed

- Explicitly load backends until amxrt finds USP backend automatically
- Migrate to new licenses format (baf)
- Entry point reasons that are not handled should not cause a failure
- Set 'super: False' for build order config option

## Release 1.0.10 - 2021-02-08(12:50:31 +0100)

## Changed

- Issue IOT-788 [LCM] mqtt client publish segfault

## Release 1.0.9 - 2021-02-04(09:50:31 +0100)

### Changed

- Update startup command

## Release 1.0.8 - 2021-01-26(09:53:36 +0000)

### Changed

- Use tagged version of `libmosquitto_poc`

## Release 1.0.7 - 2021-01-20(09:22:02 +0000)

### Changed

- Update install location

## Release 1.0.6 - 2021-01-18(12:47:07 +0000)

### Changed

- Rename repository `libusp_com` to `libimtp`

## Release 1.0.5 - 2021-01-14(09:37:37 +0000)

### Added

- Add test for incoming message on unix domain socket message

### Changed

- Allocate an extra byte for the topic string to take the null-termination into account
- Make sure all memory is cleared in `test_receive_and_send_event`
- Create symbolic link to amxrt on installation
- Update pipeline variables
- Add baf

## Release 1.0.4 - 2020-12-09(07:34:00 +0000)

### Added

- [LCM] extend mqtt client functionality for low-level API client

## Release 1.0.3 - 2020-11-10(10:03:33 +0000)

### Fixed

- Take tlv offsets into account

## Release 1.0.2 - 2020-11-02(07:33:36 +0000)

### Added

- Feature TLS authentication using certificates

## Release 1.0.1 - 2020-10-29(15:19:42 +0000)

### Changed

- Update odl files with latest amxrt implementations

## Release 1.0.0 - 2020-10-27(19:36:40 +0100)

### Added

- Communication with the USP agent is implemented by sending TLVs over unix domain sockets. This is implemented in the imtp (libusp_com).
- The g++ build job is added and compilation is successful.
- Logging with sahtrace has been added. The mod_sahtrace module is used to configure logging through the odl.
- Default data model parameters have been added to the odl
- Support for user properties in the CONNACK has been added.

### Changed

- Makefiles have been updated to represent more generic install locations and component naming.
- The new codestyle has been implemented.
- Data model access to the USP agent's data model has been removed. Only the USP agent is allowed to request data model information from the MQTT client, not the other way around.

## Release 0.0.3 - 2020-08-21(19:42:27 +0000)

### Changed

- Updates code for API change

## Release 0.0.2 - 2020-08-20(15:50:37 +0000)

### Changed

- Re-organized install target - this to support multiple start-up options
- Moves dummy backend for testing to common dir
- Re-organize event testing

### Fixed

- Updates expression builder, do not use search_path function if path is not a search_path
- Fixes TOC of README.md

## Release 0.0.1 - 2020-08-16(15:00:08 +0000)

### New

- Initial version - see README.md
