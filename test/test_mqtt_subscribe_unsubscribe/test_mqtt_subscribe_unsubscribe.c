/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"

#include "mock.h"
#include "mosquitto_mock.h"
#include "test_mqtt_subscribe_unsubscribe.h"
#include "test_common.h"

static amxo_parser_t parser;
static const char* odl_defs = "../../odl/tr181-mqtt_definition.odl";

static int event_count = 0;
static void notification_handler(const char* const sig_name,
                                 UNUSED const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    printf("received notification [%s]\n", sig_name);
    event_count++;
}

int test_mqtt_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* client = NULL;
    amxd_dm_t* dm = mock_init_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    int retval = 0;

    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(amxd_trans_init(&trans), 0);
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);
    amxc_var_init(&params);

    amxo_resolver_ftab_add(&parser, "mqtt_instance_is_valid", AMXO_FUNC(_mqtt_instance_is_valid));
    amxo_resolver_ftab_add(&parser, "mqtt_instance_cleanup", AMXO_FUNC(_mqtt_instance_cleanup));

    amxo_resolver_ftab_add(&parser, "mqtt_client_toggled", AMXO_FUNC(_mqtt_client_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_disabled", AMXO_FUNC(_mqtt_client_disabled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_added", AMXO_FUNC(_mqtt_client_added));
    amxo_resolver_ftab_add(&parser, "mqtt_client_auto_reconnect", AMXO_FUNC(_mqtt_client_auto_reconnect));

    amxo_resolver_ftab_add(&parser, "mqtt_subscription_cleanup", AMXO_FUNC(_mqtt_subscription_cleanup));

    amxo_resolver_ftab_add(&parser, "mqtt_subscription_enabled", AMXO_FUNC(_mqtt_subscription_enabled));
    amxo_resolver_ftab_add(&parser, "mqtt_subscription_disabled", AMXO_FUNC(_mqtt_subscription_disabled));
    amxo_resolver_ftab_add(&parser, "mqtt_subscription_added", AMXO_FUNC(_mqtt_subscription_added));

    amxo_resolver_ftab_add(&parser, "print_event", AMXO_FUNC(_print_event));

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    assert_int_equal(retval, 0);

    capture_sigalrm();
    handle_events();

    will_return(__wrap_amxb_listen, -1); // setup direct pcb listen socket
    assert_int_equal(_mqtt_main(0, dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "MQTT.Client.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "BrokerAddress", "broker.hivemq.com");
    amxd_trans_set_value(cstring_t, &trans, "Name", "MQTT-Test1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "BrokerAddress", "localhost");
    amxd_trans_set_value(cstring_t, &trans, "Name", "MQTT-Test2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any_count(__wrap_mosquitto_new, id, 2);
    expect_any_count(__wrap_mosquitto_new, clean_session, 2);
    expect_any_count(__wrap_mosquitto_new, obj, 2);
    will_return_always(__wrap_mosquitto_new, true);

    expect_any_count(__wrap_mosquitto_int_option, mosq, 2);
    expect_value_count(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION, 2);
    expect_value_count(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5, 2);

    will_return_count(__wrap_ares_gethostbyname, 0, 2);
    expect_any_count(__wrap_mosquitto_connect_bind_async_v5, mosq, 2);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "localhost");
    expect_value_count(__wrap_mosquitto_connect_bind_async_v5, port, 1883, 2);
    expect_value_count(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60, 2);
    expect_any_count(__wrap_mosquitto_connect_bind_async_v5, bind_address, 2);
    expect_any_count(__wrap_mosquitto_connect_bind_async_v5, properties, 2);
    will_return_count(__wrap_mosquitto_connect_bind_async_v5, 0, 2);

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    assert_int_equal(amxd_object_get_params(client, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.2.");
    assert_int_equal(amxd_object_get_params(client, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
    return 0;
}

int test_mqtt_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_get_dm();

    assert_int_equal(_mqtt_main(1, dm, &parser), 0);
    expect_any_count(__wrap_mosquitto_destroy, mosq, 3);

    amxo_parser_clean(&parser);
    mock_cleanup_dm();

    return 0;
}

void test_can_create_subscription_on_enabled_client(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* subs = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.Subscription.");
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, subs);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/test");
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);
    instance = amxd_object_get(subs, "1");
    assert_non_null(instance);

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribed");

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribed");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_when_enabled_subscription_is_taken(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.Subscription.1.");

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);

    expect_any(__wrap_mosquitto_subscribe_v5, mosq);
    expect_any(__wrap_mosquitto_subscribe_v5, mid);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/test");
    expect_value(__wrap_mosquitto_subscribe_v5, qos, 0);
    expect_value(__wrap_mosquitto_subscribe_v5, options, 0);
    expect_any(__wrap_mosquitto_subscribe_v5, properties);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribing");

    mosquitto_mock_finalize_sub(instance);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribed");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_when_disabled_subscription_is_removed(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* instance = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.1.");

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any(__wrap_mosquitto_unsubscribe_v5, mosq);
    expect_any(__wrap_mosquitto_unsubscribe_v5, mid);
    expect_string(__wrap_mosquitto_unsubscribe_v5, sub, "mqtt/client/test");
    expect_any(__wrap_mosquitto_unsubscribe_v5, properties);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribing");

    mosquitto_mock_finalize_unsub(instance);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribed");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_disable_client(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = mock_get_dm();

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);

    handle_events();

    expect_any(__wrap_mosquitto_destroy, mosq);

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxd_trans_clean(&trans);
}

void test_add_subscriptions_on_disabled_client_does_not_subscribe(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* subs = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.");
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.Subscription.1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, "MQTT.Client.1.Subscription.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/e-sub-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/e-sub-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/e-sub-3");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    amxd_object_for_each(instance, it, subs) {
        amxd_object_t* sub = amxc_llist_it_get_data(it, amxd_object_t, it);
        assert_int_equal(amxd_object_get_params(sub, &params, amxd_dm_access_protected), 0);
        amxc_var_dump(&params, STDOUT_FILENO);
        assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribed");
    }

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_enable_client_subscribes_all_enable_subsriptions(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* sub1 = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.1.");
    amxd_object_t* sub3 = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.3.");

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return_always(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    expect_any_count(__wrap_mosquitto_subscribe_v5, mosq, 2);
    expect_any_count(__wrap_mosquitto_subscribe_v5, mid, 2);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/test");
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/e-sub-2");
    expect_value_count(__wrap_mosquitto_subscribe_v5, qos, 0, 2);
    expect_value_count(__wrap_mosquitto_subscribe_v5, options, 0, 2);
    expect_any_count(__wrap_mosquitto_subscribe_v5, properties, 2);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    handle_events();

    assert_int_equal(amxd_object_get_params(sub1, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribing");
    mosquitto_mock_finalize_sub(sub1);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    assert_int_equal(amxd_object_get_params(sub1, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribed");

    assert_int_equal(amxd_object_get_params(sub3, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribing");
    mosquitto_mock_finalize_sub(sub3);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    assert_int_equal(amxd_object_get_params(sub3, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribed");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_add_enabled_subsriptions_on_enabled_client_subscribes(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* sub = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.Subscription.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/e-sub-4");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    sub = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.5.");

    expect_any(__wrap_mosquitto_subscribe_v5, mosq);
    expect_any(__wrap_mosquitto_subscribe_v5, mid);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/e-sub-4");
    expect_value(__wrap_mosquitto_subscribe_v5, qos, 0);
    expect_value(__wrap_mosquitto_subscribe_v5, options, 0);
    expect_any(__wrap_mosquitto_subscribe_v5, properties);

    handle_events();

    assert_int_equal(amxd_object_get_params(sub, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribing");
    mosquitto_mock_finalize_sub(sub);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    assert_int_equal(amxd_object_get_params(sub, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Subscribed");

    handle_events();

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_disable_client_unsubscribes_reset_subscribed_subsriptions(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* subs = amxd_dm_findf(dm, "MQTT.Client.1.Subscription.");
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);

    handle_events();

    expect_any(__wrap_mosquitto_destroy, mosq);

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    handle_events();

    amxd_object_for_each(instance, it, subs) {
        amxd_object_t* sub = amxc_llist_it_get_data(it, amxd_object_t, it);
        assert_int_equal(amxd_object_get_params(sub, &params, amxd_dm_access_protected), 0);
        amxc_var_dump(&params, STDOUT_FILENO);
        assert_string_equal(GET_CHAR(&params, "Status"), "Unsubscribed");
    }

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_inflight_subscribes_are_cleaned_up(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = mock_get_dm();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return_always(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    expect_any_count(__wrap_mosquitto_subscribe_v5, mosq, 3);
    expect_any_count(__wrap_mosquitto_subscribe_v5, mid, 3);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/test");
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/e-sub-2");
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/e-sub-4");
    expect_value_count(__wrap_mosquitto_subscribe_v5, qos, 0, 3);
    expect_value_count(__wrap_mosquitto_subscribe_v5, options, 0, 3);
    expect_any_count(__wrap_mosquitto_subscribe_v5, properties, 3);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.1.Subscription.");
    amxd_trans_del_inst(&trans, 1, NULL);
    amxd_trans_del_inst(&trans, 3, NULL);
    amxd_trans_del_inst(&trans, 5, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    amxd_trans_clean(&trans);
}


void test_receive_and_send_event(UNUSED void** state) {
    amxd_object_t* client_obj = NULL;
    mqtt_client_t* client = NULL;
    amxd_trans_t trans;
    amxd_dm_t* dm;

    client_obj = amxd_dm_findf(mock_get_dm(), "MQTT.Client.2.");
    assert_non_null(client_obj);
    assert_non_null(client_obj->priv);

    expect_any(__wrap_mosquitto_subscribe_v5, mosq);
    expect_any(__wrap_mosquitto_subscribe_v5, mid);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "mqtt/client/eventtest");
    expect_value(__wrap_mosquitto_subscribe_v5, qos, 0);
    expect_value(__wrap_mosquitto_subscribe_v5, options, 0);
    expect_any(__wrap_mosquitto_subscribe_v5, properties);

    dm = mock_get_dm();
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, "MQTT.Client.2.Subscription.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Topic", "mqtt/client/eventtest");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxp_slot_connect(&dm->sigmngr, "mqtt", NULL, notification_handler, NULL);

    client = (mqtt_client_t*) client_obj->priv;
    mock_send_message(client->con->lib_data, client->con, "mqtt/client/eventtest", "{\"payload\":\"test\"}");

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    handle_events();

    assert_int_equal(event_count, 1);

    amxd_trans_clean(&trans);
}

/**
   In this test we create a new enabled Client instance with AutoReconnect = true and
   CleanStart = false. We add an enabled Subscription instance for the Client and check that the
   Subscription Status goes to Subscribed. Then we change the BrokerAddress to toggle the automatic
   disconnect from the first broker and connect to the new broker. After we handle all events, the
   subscriptions must have been recreated. We check the Subscription Status to make sure the
   subscription is recreated.
 */
void test_client_auto_reconnect_no_clean_start(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_trans_t trans;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    amxd_object_t* subscription = NULL;
    amxc_string_t client_path;
    amxc_string_t subs_path;
    const char* new_broker = "test.mosquitto.org";
    const char* name = "MQTT-Test-clean-start";

    amxc_string_init(&client_path, 0);
    amxc_string_init(&subs_path, 0);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    // Add Enabled Client with AutoReconnect = true and CleanStart = false
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "5.0");
    amxc_var_add_key(bool, params, "CleanStart", false);
    amxc_var_add_key(bool, params, "AutoReconnect", true);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_string_setf(&client_path, "MQTT.Client.%d.", GET_UINT32(&ret, "index"));
    client = amxd_dm_findf(mock_get_dm(), "%s", amxc_string_get(&client_path, 0));
    assert_non_null(client);
    assert_null(client->priv); // events not yet handled

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    // Handling events will add Client to data model
    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connecting");

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_connect

    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    // Add subscription instance
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "Topic", "topic-auto-reconnect");

    subscription = amxd_dm_findf(mock_get_dm(), "%sSubscription.", amxc_string_get(&client_path, 0));
    assert_non_null(subscription);
    assert_int_equal(amxd_object_invoke_function(subscription, "_add", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_string_setf(&subs_path, "%sSubscription.%d.", amxc_string_get(&client_path, 0), GET_UINT32(&ret, "index"));
    subscription = amxd_dm_findf(mock_get_dm(), "%s", amxc_string_get(&subs_path, 0));
    assert_non_null(subscription);

    expect_any(__wrap_mosquitto_subscribe_v5, mosq);
    expect_any(__wrap_mosquitto_subscribe_v5, mid);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "topic-auto-reconnect");
    expect_value(__wrap_mosquitto_subscribe_v5, qos, 0);
    expect_value(__wrap_mosquitto_subscribe_v5, options, 0);
    expect_any(__wrap_mosquitto_subscribe_v5, properties);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(subscription, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&subs_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Subscribing");

    mosquitto_mock_finalize_sub(subscription);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_subscribe

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(subscription, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&subs_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Subscribed");

    // Change the BrokerAddress and reconnect
    amxd_trans_select_pathf(&trans, "%s", amxc_string_get(&client_path, 0));
    amxd_trans_set_value(cstring_t, &trans, "BrokerAddress", new_broker);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, new_broker);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_disconnect

    // Client is Disabled and subscription is Unsubscribed
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Disabled");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(subscription, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&subs_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Unsubscribed");

    expect_any(__wrap_mosquitto_subscribe_v5, mosq);
    expect_any(__wrap_mosquitto_subscribe_v5, mid);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "topic-auto-reconnect");
    expect_value(__wrap_mosquitto_subscribe_v5, qos, 0);
    expect_value(__wrap_mosquitto_subscribe_v5, options, 0);
    expect_any(__wrap_mosquitto_subscribe_v5, properties);

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_connect

    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    mosquitto_mock_finalize_sub(subscription);
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_subscribe

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(subscription, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&subs_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Subscribed");

    amxc_string_clean(&subs_path);
    amxc_string_clean(&client_path);
    amxd_trans_clean(&trans);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
