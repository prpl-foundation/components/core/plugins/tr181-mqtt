MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)
COMMON_SRC_DIR = ../test_common

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) $(wildcard $(COMMON_SRC_DIR)/*.c)

MOCK_WRAP = mosquitto_lib_init \
			mosquitto_lib_cleanup \
			mosquitto_int_option \
			mosquitto_connect_v5_callback_set \
			mosquitto_disconnect_v5_callback_set \
			mosquitto_publish_v5_callback_set \
			mosquitto_message_v5_callback_set \
			mosquitto_subscribe_v5_callback_set \
			mosquitto_unsubscribe_v5_callback_set \
			mosquitto_log_callback_set \
			mosquitto_string_to_property_info \
			mosquitto_user_data_set \
			mosquitto_property_add_byte \
			mosquitto_property_add_string_pair \
			mosquitto_property_add_string \
			mosquitto_loop_read \
			mosquitto_socket \
			mosquitto_connect_bind_async_v5 \
			mosquitto_property_free_all \
			mosquitto_disconnect_v5 \
			mosquitto_subscribe_v5 \
			mosquitto_unsubscribe_v5 \
			mosquitto_publish_v5 \
			mosquitto_new \
			mosquitto_destroy \
			mosquitto_username_pw_set \
			mosquitto_topic_matches_sub \
			mosquitto_tls_set \
			mosquitto_tls_insecure_set \
			mosquitto_loop_misc \
			mosquitto_loop_write \
			mosquitto_send_connect \
			mosquitto_string_option \
			amxb_listen \
			amxb_accept \
			amxb_get_fd \
			amxb_free \
			netmodel_initialize \
			netmodel_cleanup \
			netmodel_openQuery_isUp \
			netmodel_closeQuery \
			getsockopt \
			ares_library_init \
			ares_library_cleanup \
			ares_init_options \
			ares_destroy \
			ares_process_fd \
			ares_gethostbyname \
			inet_ntoa \
			imtp_connection_listen \
			imtp_connection_accept \
			imtp_connection_get_fd \
			imtp_connection_get_con \
			imtp_connection_delete \
			imtp_connection_read_frame \
			imtp_connection_write_frame


WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks -I../test_common \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxj -lamxp -lamxd -lamxb -lamxo \
		   -ldl -lpthread -limtp -lsahtrace -lm 

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
