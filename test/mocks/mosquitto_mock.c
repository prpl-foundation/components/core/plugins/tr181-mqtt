/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <errno.h>
#include <cmocka.h>

#include <mosquitto.h>
#include <mqtt_protocol.h>

#include "mosquitto_mock.h"
#include "mqtt_interface.h"

static int sock_fd = 50;

struct __wrap_mosquitto {
    void* priv;
    const char* id;
    int msg_id;
    int sock;
    void (* on_connect_v5)(struct mosquitto*, void* userdata, int rc, int flags, const mosquitto_property* props);
    void (* on_disconnect_v5)(struct mosquitto*, void* userdata, int rc, const mosquitto_property* props);
    void (* on_publish_v5)(struct mosquitto*, void* userdata, int mid, int reason_code, const mosquitto_property* props);
    void (* on_message_v5)(struct mosquitto*, void* userdata, const struct mosquitto_message* message, const mosquitto_property* props);
    void (* on_subscribe_v5)(struct mosquitto*, void* userdata, int mid, int qos_count, const int* granted_qos, const mosquitto_property* props);
    void (* on_unsubscribe_v5)(struct mosquitto*, void* userdata, int mid, const mosquitto_property* props);
    void (* on_log)(struct mosquitto*, void* userdata, int level, const char* str);
};

int __wrap_mosquitto_lib_init(void) {
    return 0;
}

int __wrap_mosquitto_lib_cleanup(void) {
    return 0;
}

struct mosquitto* __wrap_mosquitto_new(const char* id,
                                       bool clean_session,
                                       void* obj) {

    check_expected(id);
    check_expected(clean_session);
    check_expected(obj);

    bool must_return = mock();
    if(must_return) {
        struct __wrap_mosquitto* wm = calloc(1, sizeof(struct __wrap_mosquitto));
        wm->priv = obj;
        wm->id = id;
        wm->sock = sock_fd++;
        return (struct mosquitto*) wm;
    } else {
        errno = 101;
        return NULL;
    }
}

void __wrap_mosquitto_destroy(struct mosquitto* mosq) {
    check_expected(mosq);
    free(mosq);
}

int __wrap_mosquitto_int_option(struct mosquitto* mosq,
                                enum mosq_opt_t option,
                                int value) {
    check_expected(mosq);
    check_expected(option);
    check_expected(value);

    return 0;
}

void __wrap_mosquitto_connect_v5_callback_set(struct mosquitto* mosq,
                                              void (* on_connect)(struct mosquitto*, void*, int, int, const mosquitto_property* props)) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_connect_v5 = on_connect;
}

void __wrap_mosquitto_disconnect_v5_callback_set(struct mosquitto* mosq,
                                                 void (* on_disconnect)(struct mosquitto*, void*, int, const mosquitto_property*)) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_disconnect_v5 = on_disconnect;
}

void __wrap_mosquitto_publish_v5_callback_set(UNUSED struct mosquitto* mosq,
                                              UNUSED void (* on_publish)(struct mosquitto*, void*, int, int, const mosquitto_property*)) {

}

void __wrap_mosquitto_message_v5_callback_set(struct mosquitto* mosq,
                                              void (* on_message)(struct mosquitto*, void*, const struct mosquitto_message*, const mosquitto_property*)) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_message_v5 = on_message;
}

void __wrap_mosquitto_subscribe_v5_callback_set(struct mosquitto* mosq,
                                                void (* on_subscribe_v5)(struct mosquitto*, void* userdata, int mid, int qos_count, const int* granted_qos, const mosquitto_property* props)) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_subscribe_v5 = on_subscribe_v5;
}

void __wrap_mosquitto_unsubscribe_v5_callback_set(struct mosquitto* mosq,
                                                  void (* on_unsubscribe_v5)(struct mosquitto*, void* userdata, int mid, const mosquitto_property* props)) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_unsubscribe_v5 = on_unsubscribe_v5;
}

void __wrap_mosquitto_log_callback_set(UNUSED struct mosquitto* mosq,
                                       UNUSED void (* on_log)(struct mosquitto*, void*, int, const char*)) {

}

int __wrap_mosquitto_string_to_property_info(const char* propname,
                                             int* identifier,
                                             int* type) {

    if(strcmp(propname, "assigned-client-identifier") == 0) {
        *identifier = MQTT_PROP_ASSIGNED_CLIENT_IDENTIFIER;
        *type = MQTT_PROP_TYPE_STRING;
    } else if(strcmp(propname, "user-property") == 0) {
        *identifier = MQTT_PROP_USER_PROPERTY;
        *type = MQTT_PROP_TYPE_STRING_PAIR;
    }
    return 0;
}

void __wrap_mosquitto_user_data_set(UNUSED struct mosquitto* mosq,
                                    UNUSED void* obj) {

}

int __wrap_mosquitto_property_add_byte(UNUSED mosquitto_property** proplist,
                                       UNUSED int identifier,
                                       UNUSED uint8_t value) {
    return 0;
}

int __wrap_mosquitto_property_add_string_pair(UNUSED mosquitto_property** proplist,
                                              UNUSED int identifier,
                                              UNUSED const char* name,
                                              UNUSED const char* value) {
    return 0;
}

int __wrap_mosquitto_property_add_string(UNUSED mosquitto_property** proplist,
                                         UNUSED int identifier,
                                         UNUSED const char* value) {
    return 0;
}

int __wrap_mosquitto_loop_read(UNUSED struct mosquitto* mosq, UNUSED int max_packets) {
    return 0;
}

int __wrap_mosquitto_socket(struct mosquitto* mosq) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    return wm->sock;
}

static void property_add(mosquitto_property** proplist, struct mqtt5__property* prop) {
    mosquitto_property* p;

    if(!(*proplist)) {
        *proplist = prop;
    }

    p = *proplist;
    while(p->next) {
        p = p->next;
    }
    p->next = prop;
    prop->next = NULL;
}

static void property_free_all(mosquitto_property** property) {
    mosquitto_property* p, * next;

    if(!property) {
        return;
    }

    p = *property;
    while(p) {
        next = p->next;
        free(p->value.s.v);
        free(p->name.v);
        free(p);
        p = next;
    }
    *property = NULL;
}


static void mosquitto_mock_add_user_prop(mosquitto_property** prop, const char* name, const char* value) {
    struct mqtt5__property* p = calloc(1, sizeof(mosquitto_property));
    p->identifier = MQTT_PROP_USER_PROPERTY;
    p->name.v = strdup(name);
    p->name.len = strlen(name);
    p->value.s.v = strdup(value);
    p->value.s.len = strlen(value);
    property_add(prop, p);
}

static void mosquitto_mock_add_string_prop(mosquitto_property** prop, int id, const char* value) {
    struct mqtt5__property* p = calloc(1, sizeof(mosquitto_property));
    p->identifier = id;
    p->value.s.v = strdup(value);
    p->value.s.len = strlen(value);
    property_add(prop, p);
}

int __wrap_mosquitto_connect_bind_async_v5(struct mosquitto* mosq,
                                           const char* host,
                                           int port,
                                           int keepalive,
                                           const char* bind_address,
                                           const mosquitto_property* properties) {
    check_expected(mosq);
    check_expected(host);
    check_expected(port);
    check_expected(keepalive);
    check_expected(bind_address);
    check_expected(properties);

    mosquitto_property* received_props = NULL;
    int rv = mock();
    // when 0 - just return 0
    // when 1 - fill properties & return 0
    // when 2 - fail with some error code - do not call on_connect_v5 callback

    if(rv == 1) {
        mosquitto_mock_add_string_prop(&received_props, MQTT_PROP_ASSIGNED_CLIENT_IDENTIFIER, "Some-server-side-gen-client-id");
        mosquitto_mock_add_string_prop(&received_props, MQTT_PROP_RESPONSE_INFORMATION, "response/topic");
        mosquitto_mock_add_user_prop(&received_props, "subscribe-topic", "usp/server/provided/topic");
        mosquitto_mock_add_user_prop(&received_props, "subscribe-topic", "usp/server/backup/topic");
    }

    if(rv != MOSQ_ERR_CONN_INPROGRESS) {
        struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
        assert_non_null(wm->on_connect_v5);
        assert_non_null(wm->priv);

        wm->on_connect_v5(mosq, wm->priv, 0, 0, received_props);
    }
    if(rv == 1) {
        rv = 0;
        property_free_all(&received_props);
    }
    return rv;
}

void __wrap_mosquitto_property_free_all(UNUSED mosquitto_property** properties) {

}

int __wrap_mosquitto_publish_v5(UNUSED struct mosquitto* mosq,
                                UNUSED int* mid,
                                UNUSED const char* topic,
                                UNUSED int payloadlen,
                                UNUSED const void* payload,
                                UNUSED int qos,
                                UNUSED bool retain,
                                UNUSED const mosquitto_property* properties) {
    return 0;
}

int __wrap_mosquitto_disconnect_v5(struct mosquitto* mosq,
                                   int reason_code,
                                   const mosquitto_property* properties) {
    check_expected(mosq);
    check_expected(reason_code);
    check_expected(properties);

    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    assert_non_null(wm->on_disconnect_v5);
    assert_non_null(wm->priv);
    wm->on_disconnect_v5(mosq, wm->priv, 0, NULL);
    return mock();
}

void mosquitto_mock_finalize_sub(amxd_object_t* sub) {
    amxd_object_t* client = amxd_object_get_parent(amxd_object_get_parent(sub));
    mqtt_client_t* mqtt_client = client->priv;
    assert_non_null(mqtt_client);
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mqtt_client->con->lib_data;
    mqtt_sub_t* sub_ref = (mqtt_sub_t*) sub->priv;
    wm->on_subscribe_v5((struct mosquitto*) wm, wm->priv, sub_ref->message_id, 0, NULL, NULL);
}

int __wrap_mosquitto_subscribe_v5(struct mosquitto* mosq,
                                  int* mid,
                                  const char* sub,
                                  int qos,
                                  int options,
                                  const mosquitto_property* properties) {
    check_expected(mosq);
    check_expected(mid);
    check_expected(sub);
    check_expected(qos);
    check_expected(options);
    check_expected(properties);

    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    assert_non_null(wm->on_subscribe_v5);
    assert_non_null(wm->priv);
    wm->msg_id++;
    *mid = wm->msg_id;

    // must be done after returning from this function, see mosquitto_mock_finalize_sub
    //wm->on_subscribe_v5

    return 0;
}

void mosquitto_mock_finalize_unsub(amxd_object_t* sub) {
    amxd_object_t* client = amxd_object_get_parent(amxd_object_get_parent(sub));
    mqtt_client_t* mqtt_client = client->priv;
    assert_non_null(mqtt_client);
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mqtt_client->con->lib_data;
    mqtt_sub_t* sub_ref = (mqtt_sub_t*) sub->priv;
    wm->on_unsubscribe_v5((struct mosquitto*) wm, wm->priv, sub_ref->message_id, NULL);
}

int __wrap_mosquitto_unsubscribe_v5(struct mosquitto* mosq,
                                    int* mid,
                                    const char* sub,
                                    const mosquitto_property* properties) {
    check_expected(mosq);
    check_expected(mid);
    check_expected(sub);
    check_expected(properties);

    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    assert_non_null(wm->on_unsubscribe_v5);
    assert_non_null(wm->priv);
    wm->msg_id++;
    *mid = wm->msg_id;

    // must be done after returning from this function, see mosquitto_mock_finalize_unsub
    //wm->on_unsubscribe_v5

    return 0;
}

int __wrap_mosquitto_username_pw_set(UNUSED struct mosquitto* mosq,
                                     UNUSED const char* username,
                                     UNUSED const char* password) {
    return 0;
}

int __wrap_mosquitto_topic_matches_sub(UNUSED const char* sub,
                                       UNUSED const char* topic,
                                       UNUSED bool* result) {
    return 0;
}

int __wrap_mosquitto_tls_set(UNUSED struct mosquitto* mosq,
                             UNUSED const char* cafile,
                             UNUSED const char* capath,
                             UNUSED const char* certfile,
                             UNUSED const char* keyfile,
                             UNUSED int (* pw_callback)(char* buf, int size, int rwflag, void* userdata)) {
    check_expected_ptr(mosq);
    check_expected_ptr(cafile);
    check_expected_ptr(capath);
    check_expected_ptr(certfile);
    check_expected_ptr(keyfile);
    check_expected_ptr(pw_callback);
    return mock_type(int);
}

int __wrap_mosquitto_tls_insecure_set(struct mosquitto* mosq, bool value) {
    check_expected_ptr(mosq);
    check_expected(value);
    return mock_type(int);
}

int __wrap_mosquitto_loop_misc(UNUSED struct mosquitto* mosq) {
    return 0;
}

int __wrap_mosquitto_loop_write(UNUSED struct mosquitto* mosq, UNUSED int max_packets) {
    return 0;
}

int __wrap_mosquitto_send_connect(UNUSED struct mosquitto* mosq) {
    struct __wrap_mosquitto* wm = (struct __wrap_mosquitto*) mosq;
    wm->on_connect_v5(mosq, wm->priv, 0, 0, NULL);
    return 0;
}

int __wrap_mosquitto_string_option(UNUSED struct mosquitto* mosq, UNUSED enum mosq_opt_t option, UNUSED const char* value) {
    return 0;
}

void mock_send_message(void* wm, void* priv, char* topic, char* payload) {
    struct __wrap_mosquitto* cwm = (struct __wrap_mosquitto*) wm;
    struct mosquitto_message msg;
    msg.topic = topic;
    msg.payload = payload;
    msg.payloadlen = strlen(payload);
    msg.retain = false;
    msg.qos = 1;

    cwm->on_message_v5((struct mosquitto*) wm, priv, &msg, NULL);
}
