/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

#include <arpa/inet.h>

#include "mqtt_interface.h"
#include "ares_mock.h"

static char* saved_hostname = NULL;

static int ares_mock_get_next_fd(void) {
    static int fd = 555;
    return fd++;
}

int __wrap_ares_library_init(UNUSED int flags) {
    return 0;
}

void __wrap_ares_library_cleanup(void) {
    // free saved hostname on exit to clean up memory
    free(saved_hostname);
    saved_hostname = NULL;
    return;
}

int __wrap_ares_init_options(UNUSED ares_channel* channelptr,
                             UNUSED struct ares_options* options,
                             UNUSED int optmask) {
    return 0;
}

void __wrap_ares_destroy(UNUSED ares_channel channel) {
    return;
}

void __wrap_ares_process_fd(UNUSED ares_channel channel,
                            UNUSED ares_socket_t read_fd,
                            UNUSED ares_socket_t write_fd) {
    return;
}

/**
   This function should call the provided callback function when the hostname has been resolved
   to an IP address. The callback function will then use the resolved IP to call the mqtt_connect
   function.
   During the tests we will assume that the name adres translations from c-ares work fine and just
   return the broker address again in __wrap_inet_ntoa.
 */
void __wrap_ares_gethostbyname(UNUSED ares_channel channel,
                               const char* name,
                               UNUSED int family,
                               UNUSED ares_host_callback callback,
                               void* arg) {
    struct hostent host;
    char* ip = strdup(name);
    int cb_status = mock_type(int);
    amxd_object_t* object = (amxd_object_t*) arg;
    mqtt_client_t* client = NULL;

    host.h_name = strdup(name);
    host.h_length = 1;
    host.h_addr_list = calloc(1, sizeof(void*));
    host.h_addr_list[0] = ip;

    free(saved_hostname);
    saved_hostname = strdup(name);

    client = (mqtt_client_t*) object->priv;
    client->dns_query.options.sock_state_cb(client->dns_query.options.sock_state_cb_data,
                                            ares_mock_get_next_fd(),
                                            1,
                                            0);
    callback(arg, cb_status, 1, &host);

    free(host.h_name);
    free(host.h_addr_list);
    free(ip);
    return;
}

char* __wrap_inet_ntoa(UNUSED struct in_addr addr) {
    return saved_hostname;
}
