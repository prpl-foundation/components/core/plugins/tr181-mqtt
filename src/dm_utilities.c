/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "dm_mqtt_utilities.h"
#include "mqtt_interface.h"

void mqtt_client_update_status(amxd_object_t* obj,
                               const char* status) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);
}

/* Update Device.MQTT.Client.{i}.Subscription.{i}.Status
 * Possible values for the status are
 *   - Unsubscribed
 *   - Subscribed
 *   - Subscribing (optional)
 *   - Unsubscribing (optional)
 *   - Error
 */
void mqtt_subscription_update_status(amxd_object_t* obj,
                                     const char* status) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);
}

void mqtt_client_for_all(amxd_object_t* client,
                         const char* spath,
                         mqtt_client_job_t fn,
                         void* priv) {
    amxc_llist_t paths;
    amxd_dm_t* dm = mqtt_get_dm();

    amxc_llist_init(&paths);
    amxd_object_resolve_pathf(client, &paths, "%s", spath);

    amxc_llist_for_each(it, (&paths)) {
        amxc_string_t* path = amxc_string_from_llist_it(it);
        amxd_object_t* sub_obj = amxd_dm_findf(dm, "%s", amxc_string_get(path, 0));
        if(sub_obj != NULL) {
            fn(client, sub_obj, priv);
        }
    }

    amxc_llist_clean(&paths, amxc_string_list_it_free);
}

/* Function called on a datamodel event if Subscriptions are added and Enabled
 * or if a connection is made and there are Subscriptions set to Enabled
 */
int mqtt_client_subscribe(amxd_object_t* client,
                          amxd_object_t* sub,
                          void* priv) {
    int retval = -1;
    mqtt_client_t* cl = (mqtt_client_t*) client->priv;
    amxc_var_t params;
    int32_t msg_id = 0;
    amxd_trans_t* trans = (amxd_trans_t*) priv;
    mqtt_sub_t* sub_ref = NULL;
    char* topic = NULL;

    amxc_var_init(&params);
    when_null(cl, exit);
    when_null(cl->con, exit);

    if(sub->priv == NULL) {
        sub_ref = (mqtt_sub_t*) calloc(1, sizeof(mqtt_sub_t));
        sub->priv = sub_ref;
    } else {
        sub_ref = (mqtt_sub_t*) sub->priv;
    }

    amxd_object_get_params(sub, &params, amxd_dm_access_protected);

    topic = amxc_var_dyncast(cstring_t, GET_ARG(&params, "Topic"));
    when_str_empty(topic, exit);

    retval = mqtt_subscribe(cl->con,
                            &msg_id,
                            topic,
                            GET_UINT32(&params, "QoS"),
                            0);

    if(trans != NULL) {
        const char* s = (retval == 0) ? "Subscribing" : "Error";
        amxd_trans_select_object(trans, sub);
        amxd_trans_set_value(cstring_t, trans, "Status", s);
    }

    sub_ref->sub_obj = sub;
    free(sub_ref->topic);
    sub_ref->topic = topic;
    sub_ref->message_id = msg_id;
    amxc_llist_append(&(cl->con->subs), &sub_ref->it);

    if(retval == 0) {
        sub_ref->status = mqtt_sub_status_subscribing;
    } else {
        sub_ref->status = mqtt_sub_status_error;
    }

exit:
    amxc_var_clean(&params);
    return retval;
}

/* Function called on data model subscription disable event */
int mqtt_client_unsubscribe(amxd_object_t* client,
                            amxd_object_t* sub,
                            UNUSED void* priv) {
    int retval = -1;
    mqtt_client_t* cl = (mqtt_client_t*) client->priv;
    amxc_var_t params;
    mqtt_sub_t* sub_ref = NULL;

    if(sub->priv == NULL) {
        sub_ref = (mqtt_sub_t*) calloc(1, sizeof(mqtt_sub_t));
        sub->priv = sub_ref;
    } else {
        sub_ref = (mqtt_sub_t*) sub->priv;
    }

    amxc_var_init(&params);
    when_null(cl, exit);

    amxd_object_get_params(sub, &params, amxd_dm_access_protected);

    retval = mqtt_unsubscribe(cl->con,
                              &sub_ref->message_id,
                              GET_CHAR(&params, "Topic"));

    SAH_TRACEZ_INFO(ME, "UNSUBSCRIBED retval = %d", retval);

    if(retval == 0) {
        sub_ref->status = mqtt_sub_status_unsubscribing;
    } else {
        sub_ref->status = mqtt_sub_status_error;
    }

exit:
    amxc_var_clean(&params);
    return retval;
}

int mqtt_client_sub_reset(UNUSED amxd_object_t* client,
                          amxd_object_t* sub,
                          void* priv) {
    amxd_trans_t* trans = (amxd_trans_t*) priv;

    amxd_trans_select_object(trans, sub);
    amxd_trans_set_value(cstring_t, trans, "Status", "Unsubscribed");

    return 0;
}
