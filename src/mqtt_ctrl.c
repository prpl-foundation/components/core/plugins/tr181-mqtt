/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <mosquitto.h>

#include "mqtt_interface.h"

typedef struct _mqtt_ctrl {
    int pipe[2];
    amxc_lqueue_t queue;
    pthread_mutex_t mutex;
    mqtt_handler_t handlers[mqtt_item_max];
} mqtt_ctrl_t;

static mqtt_ctrl_t mqtt_ctrl;

static mqtt_item_t* mqtt_ctrl_dequeue(void) {
    amxc_lqueue_it_t* sig_it = amxc_lqueue_remove(&mqtt_ctrl.queue);
    mqtt_item_t* item = NULL;
    if(sig_it != NULL) {
        item = amxc_llist_it_get_data(sig_it, mqtt_item_t, qit);
    }

    return item;
}

int mqtt_ctrl_init(void) {
    int flags = 0;
    int retval = pipe(mqtt_ctrl.pipe);
    if(retval != 0) {
        goto exit;
    }

    flags = fcntl(mqtt_ctrl.pipe[0], F_GETFL, 0);
    if(flags < 0) {
        goto exit;
    }
    if(fcntl(mqtt_ctrl.pipe[0], F_SETFL, flags | O_NONBLOCK) < 0) {
        goto exit;
    }

    amxc_lqueue_init(&mqtt_ctrl.queue);
    pthread_mutex_init(&mqtt_ctrl.mutex, NULL);

    retval = 0;

exit:
    if(retval != 0) {
        if(mqtt_ctrl.pipe[0] != -1) {
            close(mqtt_ctrl.pipe[0]);
            close(mqtt_ctrl.pipe[1]);
        }
        mqtt_ctrl.pipe[0] = -1;
        mqtt_ctrl.pipe[1] = -1;
    }
    return retval;
}

void mqtt_ctrl_clean(void) {
    mqtt_item_t* item = NULL;
    char buffer[1];

    amxc_llist_for_each(it, (&mqtt_ctrl.queue)) {
        item = amxc_llist_it_get_data(it, mqtt_item_t, qit);
        amxc_var_clean(&item->data);
        amxc_llist_it_take(&item->lit);
        amxc_llist_it_take(&item->qit);
        free(item);
        ssize_t s = read(mqtt_ctrl.pipe[0], buffer, 1);
        if(!s) {
            fprintf(stderr, "read error\n");
        }
    }

    amxo_connection_remove(mqtt_get_parser(), mqtt_ctrl_fd());
    if(mqtt_ctrl.pipe[0] != -1) {
        close(mqtt_ctrl.pipe[0]);
    }
    if(mqtt_ctrl.pipe[1] != -1) {
        close(mqtt_ctrl.pipe[1]);
    }

    pthread_mutex_destroy(&mqtt_ctrl.mutex);
}

int mqtt_ctrl_read(mqtt_con_t** con,
                   mqtt_item_t** item) {
    int retval = -1;
    int read_length = 0;
    char buffer[1];

    mqtt_ctrl_lock();
    read_length = read(mqtt_ctrl.pipe[0], buffer, 1);
    if(read_length < 1) {
        goto exit;
    }
    // Retrieve mqtt_item_t from the queue
    *item = mqtt_ctrl_dequeue();
    when_null((*item), exit);

    // Find the mqtt_con_t that corresponds to the item you got from the queue
    *con = amxc_container_of((*item)->lit.llist, mqtt_con_t, items);
    amxc_llist_it_take(&(*item)->lit);

    retval = 0;

exit:
    mqtt_ctrl_unlock();
    return retval;
}

int mqtt_ctrl_write(mqtt_con_t* con,
                    mqtt_item_t* item) {
    int retval = -1;
    int write_length = 0;

    mqtt_ctrl_lock();

    // Trigger event loop by writing a byte to the ctrl pipe
    write_length = write(mqtt_ctrl.pipe[1], "I", 1);
    if(write_length != 1) {
        goto exit;
    }

    amxc_llist_append(&con->items, &item->lit);
    amxc_lqueue_add(&mqtt_ctrl.queue, &item->qit);

exit:
    mqtt_ctrl_unlock();
    return retval;
}

int mqtt_ctrl_fd(void) {
    return mqtt_ctrl.pipe[0];
}

int mqtt_ctrl_lock(void) {
    return pthread_mutex_lock(&mqtt_ctrl.mutex);
}

int mqtt_ctrl_unlock(void) {
    return pthread_mutex_unlock(&mqtt_ctrl.mutex);
}

void mqtt_ctrl_handle_item(UNUSED int fd, UNUSED void* priv) {
    mqtt_item_t* item = NULL;
    mqtt_con_t* con = NULL;

    if(mqtt_ctrl_read(&con, &item) == 0) {
        amxc_var_t* type = GET_ARG(&item->data, "type");
        uint32_t type_val = amxc_var_constcast(uint32_t, type);
        amxc_var_delete(&type);
        if((type_val < mqtt_item_max) &&
           ( mqtt_ctrl.handlers[type_val] != NULL)) {
            mqtt_ctrl.handlers[type_val](con, item);
        }
        amxc_var_clean(&item->data);
        free(item->payload);
        free(item);
    }
}

void mqtt_ctrl_set_handler(mqtt_item_type_t id, mqtt_handler_t fn) {
    mqtt_ctrl.handlers[id] = fn;
}
