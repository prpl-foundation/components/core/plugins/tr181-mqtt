/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"
#include "mqtt_imtp.h"

void mqtt_reconnect_cb(UNUSED const char* const sig_name,
                       UNUSED const amxc_var_t* const data,
                       void* const priv) {
    amxd_object_t* object = (amxd_object_t*) priv;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_apply(&trans, mqtt_get_dm());
    amxd_trans_clean(&trans);

    amxp_slot_disconnect_with_priv(NULL, mqtt_reconnect_cb, priv);
}

amxd_status_t _Client_publish(amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    int rv = -1;
    mqtt_client_t* client = NULL;
    const char* topic = NULL;
    const char* payload = NULL;
    int msg_id = mqtt_next_msg_id();

    when_null(object, exit);

    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);

    topic = GET_CHAR(args, "topic");
    payload = GET_CHAR(args, "payload");

    SAH_TRACE_INFO("publishing to [%s]", topic);
    rv = mqtt_publish(client->con,
                      &msg_id,
                      topic,
                      strlen(payload),
                      payload,
                      1,
                      false,
                      NULL);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to publish message, rv = %d", rv);
        client->stats.pub_errors++;
        goto exit;
    }

    retval = amxd_status_ok;
exit:
    return retval;
}

amxd_status_t _Client_CreateListenSocket(amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* uri = GET_CHAR(args, "uri");
    mqtt_client_t* client = NULL;
    mqtt_imtp_t* imtp = NULL;
    amxc_var_t* topics = NULL;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "CreateListenSocket is called for uri: %s", uri);
    when_null(object, exit);

    if(object->priv == NULL) {
        client = calloc(1, sizeof(mqtt_client_t));
        amxc_llist_init(&client->imtp_cons);
        object->priv = client;
    } else {
        client = (mqtt_client_t*) object->priv;
    }

    imtp = (mqtt_imtp_t*) calloc(1, sizeof(mqtt_imtp_t));
    when_null(imtp, exit);
    imtp->client = client;
    amxc_llist_append(&client->imtp_cons, &imtp->it);

    topics = GET_ARG(args, "topics");
    if(topics != NULL) {
        imtp->topics = amxc_var_dyncast(cstring_t, topics);
        SAH_TRACEZ_INFO(ME, "Service connected to uri=%s is only interested in topics=%s", uri, imtp->topics);
    }

    retval = imtp_connection_listen(&imtp->listen_con, uri, NULL);
    when_failed(retval, exit);

    SAH_TRACEZ_INFO(ME, "Start listening for connections on fd %d", imtp_connection_get_fd(imtp->listen_con));
    amxo_connection_add(mqtt_get_parser(),
                        imtp_connection_get_fd(imtp->listen_con),
                        mqtt_imtp_connection_accept,
                        NULL,
                        (amxo_con_type_t) CON_LISTEN,
                        imtp);

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t _Client_ForceReconnect(amxd_object_t* object,
                                     amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    mqtt_client_t* client = (mqtt_client_t*) object->priv;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    when_null(client, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    when_false(strcmp(GET_CHAR(&params, "Status"), "Connected") == 0, exit);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    status = amxd_trans_apply(&trans, mqtt_get_dm());
    when_failed(status, exit);

    // TODO: provide cancel callback
    amxd_function_defer(func, &client->reconnect_id, ret, NULL, NULL);
    client->reconnect_busy = true;

    amxp_slot_connect(NULL, "mqtt:disconnected", NULL, mqtt_reconnect_cb, object);

    status = amxd_status_deferred;
exit:
    amxc_var_clean(&params);
    amxd_trans_clean(&trans);

    return status;
}

void mqtt_client_force_reconnect_failed(mqtt_client_t* client) {
    SAH_TRACEZ_INFO(ME, "ForceReconnect failed");
    amxd_function_deferred_done(client->reconnect_id, amxd_status_unknown_error, NULL, NULL);
    client->reconnect_busy = false;
}
