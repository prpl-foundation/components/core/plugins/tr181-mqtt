/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>

#include "dm_mqtt_props.h"

void dm_mqtt_props_connect(amxc_var_t* props, amxd_object_t* object) {
    const char* search_path = "UserProperty.[PacketType matches '(^.*,{1}|^)CONNECT(,{1}.*$|$)'].";
    amxd_status_t status = amxd_status_unknown_error;
    amxc_llist_t paths_list;
    SAH_TRACEZ_INFO(ME, "Adding mqtt user properties for CONNECT");

    amxc_llist_init(&paths_list);
    status = amxd_object_resolve_pathf(object, &paths_list, "%s", search_path);
    when_failed(status, exit);

    amxc_var_set_type(props, AMXC_VAR_ID_HTABLE);

    amxc_llist_iterate(it, &paths_list) {
        amxd_object_t* prop_object = NULL;
        amxc_string_t* path = amxc_string_from_llist_it(it);
        amxc_var_t params;

        amxc_var_init(&params);
        prop_object = amxd_dm_findf(mqtt_get_dm(), "%s", amxc_string_get(path, 0));
        amxd_object_get_params(prop_object, &params, amxd_dm_access_protected);
        amxc_var_add_key(cstring_t, props, GET_CHAR(&params, "Name"), GET_CHAR(&params, "Value"));

        SAH_TRACEZ_INFO(ME, "Adding user property with name %s and value %s in connect message",
                        GET_CHAR(&params, "Name"), GET_CHAR(&params, "Value"));
        amxc_var_clean(&params);
    }

exit:
    amxc_llist_clean(&paths_list, amxc_string_list_it_free);
}
