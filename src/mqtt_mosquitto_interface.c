/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <debug/sahtrace.h>
#include <mosquitto.h>
#include <mqtt_protocol.h>

#include "mqtt_interface.h"
#include "dm_mqtt_utilities.h"
#include "mqtt_client_connect.h"

static const char* engine_id = NULL;

static const char* mqtt_property_id_to_string(int id) {
    const char* rv = NULL;
    switch(id) {
    case MQTT_PROP_PAYLOAD_FORMAT_INDICATOR:
        rv = "payload-format-indicator";
        break;
    case MQTT_PROP_MESSAGE_EXPIRY_INTERVAL:
        rv = "message-expiry-interval";
        break;
    case MQTT_PROP_CONTENT_TYPE:
        rv = "content-type";
        break;
    case MQTT_PROP_RESPONSE_TOPIC:
        rv = "response-topic";
        break;
    case MQTT_PROP_CORRELATION_DATA:
        rv = "correlation-data";
        break;
    case MQTT_PROP_SUBSCRIPTION_IDENTIFIER:
        rv = "subscription-identifier";
        break;
    case MQTT_PROP_SESSION_EXPIRY_INTERVAL:
        rv = "session-expiry-interval";
        break;
    case MQTT_PROP_ASSIGNED_CLIENT_IDENTIFIER:
        rv = "assigned-client-identifier";
        break;
    case MQTT_PROP_SERVER_KEEP_ALIVE:
        rv = "server-keep-alive";
        break;
    case MQTT_PROP_AUTHENTICATION_METHOD:
        rv = "authentication-method";
        break;
    case MQTT_PROP_AUTHENTICATION_DATA:
        rv = "authentication-data";
        break;
    case MQTT_PROP_REQUEST_PROBLEM_INFORMATION:
        rv = "request-problem-information";
        break;
    case MQTT_PROP_WILL_DELAY_INTERVAL:
        rv = "will-delay-interval";
        break;
    case MQTT_PROP_REQUEST_RESPONSE_INFORMATION:
        rv = "request-response-information";
        break;
    case MQTT_PROP_RESPONSE_INFORMATION:
        rv = "response-information";
        break;
    case MQTT_PROP_SERVER_REFERENCE:
        rv = "server-reference";
        break;
    case MQTT_PROP_REASON_STRING:
        rv = "reason-string";
        break;
    case MQTT_PROP_RECEIVE_MAXIMUM:
        rv = "receive-maximum";
        break;
    case MQTT_PROP_TOPIC_ALIAS_MAXIMUM:
        rv = "topic-alias-maximum";
        break;
    case MQTT_PROP_TOPIC_ALIAS:
        rv = "topic-alias";
        break;
    case MQTT_PROP_MAXIMUM_QOS:
        rv = "maximum-qos";
        break;
    case MQTT_PROP_RETAIN_AVAILABLE:
        rv = "retain-available";
        break;
    case MQTT_PROP_USER_PROPERTY:
        rv = "user-property";
        break;
    case MQTT_PROP_MAXIMUM_PACKET_SIZE:
        rv = "maximum-packet-size";
        break;
    case MQTT_PROP_WILDCARD_SUB_AVAILABLE:
        rv = "wildcard-subscription-available";
        break;
    case MQTT_PROP_SUBSCRIPTION_ID_AVAILABLE:
        rv = "subscription-identifier-available";
        break;
    case MQTT_PROP_SHARED_SUB_AVAILABLE:
        rv = "shared-subscription-available";
        break;
    }

    return rv;
}

static amxc_var_t* mqtt_prop_add(amxc_var_t* var,
                                 const char* key) {
    amxc_var_t* data = amxc_var_get_key(var, key, AMXC_VAR_FLAG_DEFAULT);
    if(data == NULL) {
        data = amxc_var_add_new_key(var, key);
    } else {
        if(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE) {
            // nothing to do
        } else if(amxc_var_type_of(data) != AMXC_VAR_ID_LIST) {
            char* value = amxc_var_take(cstring_t, data);
            amxc_var_set_type(data, AMXC_VAR_ID_LIST);
            amxc_var_push(cstring_t, amxc_var_add_new(data), value);
            data = amxc_var_add_new(data);
        } else {
            data = amxc_var_add_new(data);
        }
    }
    return data;
}

static void mqtt_prop_type_to_variant(amxc_var_t* var,
                                      const mosquitto_property* prop,
                                      int type) {

    switch(type) {
    case MQTT_PROP_TYPE_BYTE:
        amxc_var_set(int32_t, var, prop->value.i8);
        break;
    case MQTT_PROP_TYPE_INT16:
        amxc_var_set(int32_t, var, prop->value.i16);
        break;
    case MQTT_PROP_TYPE_INT32:
        amxc_var_set(int32_t, var, prop->value.i32);
        break;
    case MQTT_PROP_TYPE_VARINT:
        amxc_var_set(int32_t, var, prop->value.varint);
        break;
    case MQTT_PROP_TYPE_BINARY:
        break;
    case MQTT_PROP_TYPE_STRING: {
        char* value = (char*) calloc(1, prop->value.s.len + 1);
        memcpy(value, prop->value.s.v, prop->value.s.len);
        amxc_var_set(cstring_t, var, value);
        free(value);
    }
    break;
    case MQTT_PROP_TYPE_STRING_PAIR: {
        char* key = (char*) calloc(1, prop->name.len + 1);
        char* value = (char*) calloc(1, prop->value.s.len + 1);
        amxc_var_t* data = NULL;
        memcpy(key, prop->name.v, prop->name.len);
        memcpy(value, prop->value.s.v, prop->value.s.len);
        if(amxc_var_type_of(var) != AMXC_VAR_ID_HTABLE) {
            amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
        }
        data = mqtt_prop_add(var, key);
        amxc_var_set(cstring_t, data, value);
        free(value);
        free(key);
    }
    break;
    }
}

static void mqtt_props_to_variant(amxc_var_t* var,
                                  const mosquitto_property* props) {
    const mosquitto_property* p = props;
    const char* key = NULL;
    int type = 0;
    int id = 0;
    amxc_var_t* data = NULL;

    while(p) {
        key = mqtt_property_id_to_string(p->identifier);
        mosquitto_string_to_property_info(key, &id, &type);
        data = mqtt_prop_add(var, key);
        mqtt_prop_type_to_variant(data, p, type);
        p = p->next;
    }
}

static void mqtt_publish_add_response_info(mqtt_con_t* con, amxc_var_t* properties) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    amxc_var_t* data = amxc_var_get_key(properties, "response-topic", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t resp_info;
    const char* resp_topic = NULL;
    amxc_var_init(&resp_info);

    when_not_null(data, exit);

    amxd_object_get_param(object, "ResponseInformation", &resp_info);
    resp_topic = amxc_var_constcast(cstring_t, &resp_info);
    amxc_var_add_key(cstring_t, properties, "response-topic", resp_topic);

exit:
    amxc_var_clean(&resp_info);
    return;
}

static void mqtt5_on_connect(UNUSED struct mosquitto* ctx,
                             void* priv,
                             UNUSED int rc,
                             UNUSED int flags,
                             const mosquitto_property* props) {
    SAH_TRACEZ_INFO(ME, "connect callback");

    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_connect);

    if(props != NULL) {
        amxc_var_t* properties
            = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt5_on_disconnect(UNUSED struct mosquitto* ctx,
                                UNUSED void* priv,
                                int reason_code,
                                const mosquitto_property* props) {
    SAH_TRACEZ_INFO(ME, "disconnect callback [%d]", reason_code);

    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;
    when_null(con, exit);

    amxo_connection_remove(mqtt_get_parser(), con->mqtt_fd);

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_disconnect);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_RC, reason_code);

    if(props != NULL) {
        amxc_var_t* properties
            = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt5_on_publish(UNUSED struct mosquitto* ctx,
                             UNUSED void* priv,
                             int message_id,
                             int reason_code,
                             const mosquitto_property* props) {
    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;
    amxc_var_t* properties = NULL;

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_publish);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_MSG_ID, message_id);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_RC, reason_code);

    if(props != NULL) {
        properties = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_publish_add_response_info(con, properties);

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt5_on_message(UNUSED struct mosquitto* ctx,
                             void* priv,
                             const struct mosquitto_message* msg,
                             const mosquitto_property* props) {
    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_message);
    amxc_var_add_key(cstring_t, &item->data, ITEMF_TOPIC, msg->topic);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_QOS, msg->qos);
    amxc_var_add_key(bool, &item->data, ITEMF_RETAIN, msg->retain);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_MSG_ID, mqtt_next_msg_id());

    item->payload = calloc(1, msg->payloadlen);
    memcpy(item->payload, msg->payload, msg->payloadlen);
    item->payloadlen = msg->payloadlen;

    if(props != NULL) {
        amxc_var_t* properties
            = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt5_on_subscribe(UNUSED struct mosquitto* ctx,
                               void* priv,
                               int message_id,
                               int qos_count,
                               const int* granted_qos,
                               const mosquitto_property* props) {
    SAH_TRACEZ_INFO(ME, "subcribe callback");

    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;
    amxc_var_t* lgranted_qos = NULL;

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_subscribe);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_MSG_ID, message_id);
    lgranted_qos =
        amxc_var_add_key(amxc_llist_t, &item->data, ITEMF_GRANTED_QOS, NULL);

    for(int i = 0; i < qos_count; i++) {
        amxc_var_add(uint32_t, lgranted_qos, granted_qos[i]);
    }

    if(props != NULL) {
        amxc_var_t* properties
            = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt5_on_unsubscribe(UNUSED struct mosquitto* ctx,
                                 void* priv,
                                 int message_id,
                                 const mosquitto_property* props) {
    mqtt_item_t* item = NULL;
    mqtt_con_t* con = (mqtt_con_t*) priv;

    item = (mqtt_item_t*) calloc(1, sizeof(mqtt_item_t));
    when_null(item, exit);

    amxc_var_set_type(&item->data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_TYPE, mqtt_item_unsubscribe);
    amxc_var_add_key(uint32_t, &item->data, ITEMF_MSG_ID, message_id);

    if(props != NULL) {
        amxc_var_t* properties
            = amxc_var_add_key(amxc_htable_t, &item->data, ITEMF_PROPS, NULL);
        mqtt_props_to_variant(properties, props);
    }

    mqtt_ctrl_write(con, item);

exit:
    return;
}

static void mqtt_on_log(UNUSED struct mosquitto* ctx,
                        UNUSED void* priv,
                        UNUSED int level,
                        UNUSED const char* msg) {
    SAH_TRACEZ_INFO(ME, "LOG %d - %s", level, msg);
}

static void mqtt_free_pending(amxc_llist_it_t* it) {
    mqtt_item_t* item = amxc_llist_it_get_data(it, mqtt_item_t, lit);
    amxc_var_clean(&item->data);
    amxc_llist_it_take(&item->qit);
    free(item);
}

static void mqtt_sub_it_delete(amxc_llist_it_t* it) {
    mqtt_sub_t* sub = amxc_llist_it_get_data(it, mqtt_sub_t, it);
    free(sub->topic);
    if(sub->sub_obj != NULL) {
        sub->sub_obj->priv = NULL; // removes private data from subscription data model object
    }
    free(sub);
}

static void mqtt_handle_message(UNUSED int fd, void* priv) {
    mqtt_con_t* con = (mqtt_con_t*) priv;
    struct mosquitto* mosq = NULL;

    when_null(con, exit);
    when_null(con->lib_data, exit);

    mosq = (struct mosquitto*) con->lib_data;
    mosquitto_loop_read(mosq, 1);
exit:
    return;
}

static void mqtt_handle_loop_misc(UNUSED amxp_timer_t* timer, void* priv) {
    struct mosquitto* mosq = (struct mosquitto*) priv;

    when_null(mosq, exit);

    // mosquitto send connect (needed because TLS handshaking is not yet finished on first write attempt)
    mosquitto_loop_write(mosq, 20);

    // mosquitto client ping
    mosquitto_loop_misc(mosq);
exit:
    return;
}

static void mqtt_setup_loop_misc(mqtt_con_t* con) {
    struct mosquitto* mosq = NULL;
    amxp_timer_t* timer = NULL;

    when_null(con, exit);
    when_null(con->lib_data, exit);

    mosq = (struct mosquitto*) con->lib_data;

    amxp_timer_delete(&con->misc_timer);
    amxp_timer_new(&timer, mqtt_handle_loop_misc, mosq);
    amxp_timer_set_interval(timer, 2000);
    amxp_timer_start(timer, 2000);
    con->misc_timer = timer;
exit:
    return;
}

void mqtt_finish_connect(UNUSED int fd, void* priv) {
    mqtt_con_t* con = (mqtt_con_t*) priv;
    struct mosquitto* mosq = NULL;
    int rv = -1;
    int sock_err = -1;
    socklen_t sock_err_len = sizeof(sock_err);
    when_null(con, exit);
    when_null(con->lib_data, exit);
    mosq = con->lib_data;

    rv = getsockopt(con->mqtt_fd, SOL_SOCKET, SO_ERROR, (void*) (&sock_err), &sock_err_len);
    if(rv < 0) {
        SAH_TRACEZ_ERROR(ME, "sock opt error: %s", strerror(rv));
        con->lib_data = NULL;
        mqtt_delete(&con);
        goto exit;
    } else {
        if(sock_err != 0) {
            SAH_TRACEZ_ERROR(ME, "sock opt error: %s", strerror(sock_err));
            goto err;
        } else {
            SAH_TRACEZ_INFO(ME, "Send CONNECT message");
            rv = mosquitto_send_connect(mosq);
            if(rv != 0) {
                SAH_TRACEZ_ERROR(ME, "Error sending CONNECT message %s", mosquitto_strerror(rv));
                goto err;
            } else {
                amxd_object_t* object = (amxd_object_t*) con->client_inst;
                mqtt_setup_loop_misc(con);

                // CONNECT is sent, but there is no guarantee the broker will respond, so start retry mechanism
                mqtt_client_reconnect_update_cb(object);
                return;
            }
        }
    }
err:
    mqtt_disconnect(con, 0);
exit:
    return;
}

void mqtt_init_lib(void) {
    mosquitto_lib_init();
    mqtt_ctrl_init();

    engine_id = getenv("DEFAULT_SSL_ENGINE_ID");
    if((engine_id == NULL) || (engine_id[0] == '\0')) {
        SAH_TRACEZ_WARNING(ME, "Engine name fallback value is used, you should set environment variables");
        engine_id = "ssle-client";
    }
}

void mqtt_cleanup_lib(void) {
    mqtt_ctrl_clean();
    mosquitto_lib_cleanup();
}

int mqtt_new(mqtt_con_t** con,
             const char* client_id,
             bool clean_session,
             const char* mqtt_version,
             void* priv) {
    int retval = -1;
    *con = (mqtt_con_t*) calloc(1, sizeof(mqtt_con_t));
    when_null(*con, exit);

    if((client_id != NULL) && (*client_id == 0)) {
        client_id = NULL;
    }

    (*con)->client_inst = priv;
    (*con)->lib_data = mosquitto_new(client_id, clean_session, *con);
    if((*con)->lib_data == NULL) {
        SAH_TRACEZ_ERROR(ME, "mosquitto_new() failed: Error: %s", strerror(errno));
        retval = errno;
        goto exit;
    }

    // Initialize linked list of mqtt_item_t and mqqt_sub_t
    amxc_llist_init(&(*con)->items);
    amxc_llist_init(&(*con)->subs);

    if(strcmp(mqtt_version, "5.0") == 0) {
        mosquitto_int_option((struct mosquitto*) (*con)->lib_data, MOSQ_OPT_PROTOCOL_VERSION, MQTT_PROTOCOL_V5);
    } else if(strcmp(mqtt_version, "3.1") == 0) {
        mosquitto_int_option((struct mosquitto*) (*con)->lib_data, MOSQ_OPT_PROTOCOL_VERSION, MQTT_PROTOCOL_V31);
    }

    mosquitto_connect_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_connect);
    mosquitto_disconnect_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_disconnect);
    mosquitto_publish_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_publish);
    mosquitto_message_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_message);
    mosquitto_subscribe_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_subscribe);
    mosquitto_unsubscribe_v5_callback_set((struct mosquitto*) (*con)->lib_data, mqtt5_on_unsubscribe);

    mosquitto_log_callback_set((struct mosquitto*) (*con)->lib_data, mqtt_on_log);

    retval = 0;

exit:
    if(retval != 0) {
        free(*con);
        *con = NULL;
    }
    return retval;
}

void mqtt_delete(mqtt_con_t** con) {
    struct mosquitto* mosq = NULL;

    if((con != NULL) && (*con != NULL)) {
        if((*con)->lib_data != NULL) {
            mosq = (struct mosquitto*) (*con)->lib_data;
            mosquitto_user_data_set(mosq, NULL);
            mosquitto_destroy(mosq);
        }
        amxp_timer_delete(&(*con)->misc_timer);
        (*con)->misc_timer = NULL;

        mqtt_ctrl_lock();
        amxc_llist_clean(&(*con)->items, mqtt_free_pending);
        amxc_llist_clean(&(*con)->subs, mqtt_sub_it_delete);
        (*con)->client_inst = NULL;
        (*con)->lib_data = NULL;
        mqtt_ctrl_unlock();

        amxo_connection_remove(mqtt_get_parser(), (*con)->mqtt_fd);

        free(*con);
        *con = NULL;
    }
}

int mqtt_tls_setup(mqtt_con_t* con, amxc_var_t* params) {
    int rv = -1;
    const char* cafile = NULL;
    const char* certfile = NULL;
    const char* keyfile = NULL;
    bool checkserverhostname = true;
    bool enablessleengine = false;

    when_null(params, exit);
    when_null(con, exit);
    when_null(con->lib_data, exit);

    cafile = GET_CHAR(params, "CACertificate");
    certfile = GET_CHAR(params, "ClientCertificate");
    keyfile = GET_CHAR(params, "PrivateKey");
    checkserverhostname = GET_BOOL(params, "CheckServerHostName");
    enablessleengine = GET_BOOL(params, "EnableSSLEEngine");

    if((cafile != NULL) && !*cafile) {
        cafile = NULL;
    } else {
        SAH_TRACEZ_INFO(ME, "cafile not empty %s", cafile);
    }

    if((certfile != NULL) && !*certfile) {
        certfile = NULL;
    } else {
        SAH_TRACEZ_INFO(ME, "certfile not empty %s", certfile);
    }
    if((keyfile != NULL) && !*keyfile) {
        keyfile = NULL;
    } else {
        SAH_TRACEZ_INFO(ME, "keyfile not empty %s", keyfile);
    }

    rv = mosquitto_tls_insecure_set((struct mosquitto*) con->lib_data, !checkserverhostname);
    if(rv != MOSQ_ERR_SUCCESS) {
        SAH_TRACEZ_ERROR(ME, "tls insecure set: Error: %s", mosquitto_strerror(rv));
    }

    rv = mosquitto_tls_set((struct mosquitto*) con->lib_data,
                           cafile,
                           NULL,
                           certfile,
                           keyfile,
                           NULL);
    if(rv) {
        if(rv == MOSQ_ERR_INVAL) {
            SAH_TRACEZ_ERROR(ME, "tls set: file not found");
            goto exit;
        } else {
            SAH_TRACEZ_ERROR(ME, "tls set: Error: %s", mosquitto_strerror(rv));
            goto exit;
        }
    } else {
        SAH_TRACEZ_INFO(ME, "tls parameters set successfully");
    }

    if(enablessleengine) {
        rv = mosquitto_string_option((struct mosquitto*) con->lib_data, MOSQ_OPT_TLS_ENGINE, engine_id);
        if(rv != MOSQ_ERR_SUCCESS) {
            SAH_TRACEZ_ERROR(ME, "Could not set SSL engine");
            goto exit;
        } else {
            SAH_TRACEZ_INFO(ME, "TLS Engine set successfully done");
        }

        rv = mosquitto_string_option((struct mosquitto*) con->lib_data, MOSQ_OPT_TLS_KEYFORM, "engine");
        if(rv != MOSQ_ERR_SUCCESS) {
            SAH_TRACEZ_ERROR(ME, "Could not set TLS keyform");
        } else {
            SAH_TRACEZ_INFO(ME, "TLS Keyform set successfully done");
        }
    }
exit:
    return rv;
}

int mqtt_connect(mqtt_con_t* con,
                 const char* host,
                 int port,
                 int keepalive,
                 const char* username,
                 const char* pwd,
                 const char* proto_version,
                 uint32_t tcp_send_mem,
                 amxc_var_t* user_props) {
    int rv = 0;
    const amxc_htable_t* hprops = amxc_var_constcast(amxc_htable_t, user_props);
    struct mosquitto* mosq = NULL;

    when_null(con, exit);
    when_null(con->lib_data, exit);

    mosq = (struct mosquitto*) con->lib_data;
    mosquitto_property* proplist = NULL;

    if(((username != NULL) && (*username == 0))) {
        username = NULL;
        pwd = NULL;
    }
    mosquitto_username_pw_set(mosq, username, pwd);

    if(strcmp(proto_version, "5.0") == 0) {
        mosquitto_property_add_byte(&proplist, MQTT_PROP_REQUEST_RESPONSE_INFORMATION, 1);

        amxc_htable_for_each(it, hprops) {
            amxc_var_t* prop = amxc_var_from_htable_it(it);
            const char* key = amxc_htable_it_get_key(it);
            char* val = amxc_var_dyncast(cstring_t, prop);
            mosquitto_property_add_string_pair(&proplist, MQTT_PROP_USER_PROPERTY, key, val);
            free(val);
        }
    }
    SAH_TRACEZ_INFO(ME, "Connect to broker on %s:%d", host, port);
    rv = mosquitto_connect_bind_async_v5(mosq, host, port, keepalive, NULL, proplist);

    con->mqtt_fd = mosquitto_socket(mosq);
    SAH_TRACEZ_INFO(ME, "mqtt fd: %d", con->mqtt_fd);

    if((tcp_send_mem != 0) && (con->mqtt_fd > 0)) {
        int ret = 0;
        SAH_TRACE_INFO("(socket %d): set default tcp_send_mem to %u", con->mqtt_fd, tcp_send_mem);
        ret = setsockopt(con->mqtt_fd, SOL_SOCKET, SO_SNDBUF, &tcp_send_mem, sizeof(int));
        if(ret != 0) {
            SAH_TRACE_ERROR("setting tcp_wmem=%u failed, %s", tcp_send_mem, strerror(errno));
        }
    }

    if(rv == 0) {
        SAH_TRACEZ_INFO(ME, "mosquitto_connect_bind_async_v5 successfully done");
        amxo_connection_add(mqtt_get_parser(), con->mqtt_fd, mqtt_handle_message, NULL, AMXO_CUSTOM, con);
        mqtt_setup_loop_misc(con);
    } else if(rv == MOSQ_ERR_CONN_INPROGRESS) {
        SAH_TRACEZ_INFO(ME, "mosquitto_connect_bind_async_v5 in progress");
        amxo_connection_add(mqtt_get_parser(), con->mqtt_fd, mqtt_handle_message, NULL, AMXO_CUSTOM, con);
        amxo_connection_wait_write(mqtt_get_parser(), con->mqtt_fd, mqtt_finish_connect);
        rv = 0;
    } else {
        SAH_TRACEZ_ERROR(ME, "connect bind async Error: %s", mosquitto_strerror(rv));
        goto exit;
    }

exit:
    mosquitto_property_free_all(&proplist);
    return rv;
}

int mqtt_disconnect(mqtt_con_t* con, int reason) {
    int ret = -1;
    when_null(con, exit);
    if(con->lib_data == NULL) {
        mqtt_delete(&con);
        goto exit;
    }
    ret = mosquitto_disconnect_v5((struct mosquitto*) con->lib_data, reason, NULL);
exit:
    return ret;
}

int mqtt_subscribe(mqtt_con_t* con,
                   int* message_id,
                   const char* pattern,
                   int qos,
                   int options) {
    int ret = -1;
    when_null(con, exit);
    when_null(con->lib_data, exit);
    ret = mosquitto_subscribe_v5((struct mosquitto*) con->lib_data,
                                 message_id,
                                 pattern,
                                 qos,
                                 options,
                                 NULL);
exit:
    return ret;
}

int mqtt_unsubscribe(mqtt_con_t* con,
                     int* message_id,
                     const char* pattern) {
    int ret = -1;
    when_null(con, exit);
    when_null(con->lib_data, exit);
    ret = mosquitto_unsubscribe_v5((struct mosquitto*) con->lib_data,
                                   message_id,
                                   pattern,
                                   NULL);
exit:
    return ret;
}

int mqtt_publish(mqtt_con_t* con,
                 int* message_id,
                 const char* topic,
                 int payloadlen,
                 const void* payload,
                 int qos,
                 bool retain,
                 amxc_var_t* mqtt_props) {
    int rv = -1;
    amxc_var_t* user_props = NULL;
    mosquitto_property* proplist = NULL;
    const char* content_type = NULL;
    const char* response_topic = NULL;

    when_null(con, exit);
    when_null(con->lib_data, exit);

    user_props = GET_ARG(mqtt_props, "user-props");
    amxc_var_for_each(prop, user_props) {
        const char* key = amxc_var_key(prop);
        char* val = amxc_var_dyncast(cstring_t, prop);
        mosquitto_property_add_string_pair(&proplist, MQTT_PROP_USER_PROPERTY, key, val);
        free(val);
    }

    content_type = GET_CHAR(mqtt_props, "content-type");
    if(content_type != NULL) {
        SAH_TRACEZ_INFO(ME, "Adding content-type property: %s", content_type);
        mosquitto_property_add_string(&proplist, MQTT_PROP_CONTENT_TYPE, content_type);
    }

    response_topic = GET_CHAR(mqtt_props, "response-topic");
    if(response_topic != NULL) {
        SAH_TRACEZ_INFO(ME, "Adding response-topic property: %s", response_topic);
        mosquitto_property_add_string(&proplist, MQTT_PROP_RESPONSE_TOPIC, response_topic);
    }

    rv = mosquitto_publish_v5((struct mosquitto*) con->lib_data, message_id, topic, payloadlen,
                              payload, qos, retain, proplist);

    mosquitto_property_free_all(&proplist);

exit:
    return rv;
}

int mqtt_topic_matches_sub(const char* sub,
                           const char* topic,
                           bool* result) {
    return mosquitto_topic_matches_sub(sub, topic, result);
}
