/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _POSIX_C_SOURCE 200809L // needed for strdup
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "mqtt_interface.h"
#include "mqtt_imtp.h"

static int mqtt_imtp_send_handshake(imtp_connection_t* con) {
    int retval = -1;
    amxc_var_t* name = amxo_parser_get_config(mqtt_get_parser(), "name");
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv_handshake = NULL;
    amxc_string_t eid;
    size_t len = 0;

    amxc_string_init(&eid, 0);
    amxc_string_setf(&eid, "proto::%s", name == NULL ? "tr181-mqtt" : GET_CHAR(name, NULL));
    len = amxc_string_text_length(&eid);

    SAH_TRACEZ_INFO(ME, "Sending handshake with value '%s'", amxc_string_get(&eid, 0));
    imtp_frame_new(&frame);
    imtp_tlv_new(&tlv_handshake, imtp_tlv_type_handshake, len,
                 amxc_string_take_buffer(&eid), 0, IMTP_TLV_TAKE);
    imtp_frame_tlv_add(frame, tlv_handshake);

    retval = imtp_connection_write_frame(con, frame);

    amxc_string_clean(&eid);
    imtp_frame_delete(&frame);
    return retval;
}

static void mqtt_imtp_unpack_usp_props(const imtp_frame_t* frame, amxc_var_t* usp_props) {
    const imtp_tlv_t* tlv_json = NULL;
    char* json_buffer = NULL;

    tlv_json = imtp_frame_get_first_tlv(frame, imtp_tlv_type_mqtt_props);
    when_null(tlv_json, exit);

    json_buffer = (char*) calloc(1, tlv_json->length + 1);
    when_null(json_buffer, exit);

    memcpy(json_buffer, (uint8_t*) tlv_json->value + tlv_json->offset, tlv_json->length);

    amxc_var_set(jstring_t, usp_props, json_buffer);
    amxc_var_cast(usp_props, AMXC_VAR_ID_ANY);

exit:
    free(json_buffer);
    return;
}

static int mqtt_imtp_unpack_frame(const imtp_frame_t* frame,
                                  char** topic,
                                  char** json_string,
                                  const imtp_tlv_t** protobuf,
                                  amxc_var_t* usp_props) {
    int retval = 0;
    const imtp_tlv_t* tlv_next = NULL;

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_topic);
    if(tlv_next != NULL) {
        (*topic) = (char*) calloc(1, tlv_next->length + 1);
        when_null(*topic, exit);
        strncpy((*topic), (char*) tlv_next->value + tlv_next->offset, tlv_next->length);
    } else {
        SAH_TRACEZ_ERROR(ME, "Did not find topic in TLV chain");
        goto exit;
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_json);
    if(tlv_next != NULL) {
        SAH_TRACEZ_INFO(ME, "Found TLV of type JSON");
        (*json_string) = (char*) calloc(1, tlv_next->length + 1);
        when_null(*json_string, exit);
        strncpy((*json_string), (char*) tlv_next->value + tlv_next->offset, tlv_next->length);
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    if(tlv_next != NULL) {
        SAH_TRACEZ_INFO(ME, "Found TLV of type protobuf");
        *protobuf = tlv_next;
    }

    mqtt_imtp_unpack_usp_props(frame, usp_props);

    retval = 0;
exit:
    return retval;
}

static int mqtt_imtp_unpack_and_publish(mqtt_client_t* client, const imtp_frame_t* frame) {
    int retval = -1;
    const imtp_tlv_t* tlv_protobuf = NULL;
    char* topic = NULL;
    char* json_string = NULL;
    int msg_id = 0;
    amxc_var_t usp_props;

    amxc_var_init(&usp_props);

    retval = mqtt_imtp_unpack_frame(frame, &topic, &json_string, &tlv_protobuf, &usp_props);
    when_failed_trace(retval, exit, ERROR, "Failed to unpack IMTP frame");

    if(topic == NULL) {
        SAH_TRACEZ_WARNING(ME, "Not publishing, no topic to publish on");
        goto exit;
    }

    if(tlv_protobuf != NULL) {
        SAH_TRACEZ_INFO(ME, "Publishing protobuf on topic: %s", topic);
        msg_id = mqtt_next_msg_id();
        retval = mqtt_publish(client->con, &msg_id, topic, tlv_protobuf->length,
                              (uint8_t*) tlv_protobuf->value + tlv_protobuf->offset,
                              1, false, &usp_props);
        if(retval < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to publish message, retval = %d", retval);
            client->stats.pub_errors++;
        }
    }
    if(json_string != NULL) {
        SAH_TRACEZ_INFO(ME, "Publishing JSON string on topic: %s", topic);
        msg_id = mqtt_next_msg_id();
        retval = mqtt_publish(client->con, &msg_id, topic, strlen(json_string),
                              json_string, 1, false, &usp_props);
        if(retval < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to publish message, retval = %d", retval);
            client->stats.pub_errors++;
        }
    }

    retval = 0;
exit:
    amxc_var_clean(&usp_props);
    free(topic);
    free(json_string);
    return retval;
}

static void mqtt_imtp_accepted_read(int fd, void* priv) {
    int retval = -1;
    mqtt_imtp_t* imtp = (mqtt_imtp_t*) priv;
    imtp_frame_t* frame = NULL;
    SAH_TRACEZ_INFO(ME, "Incoming message on fd: [%d]", fd);

    retval = imtp_connection_read_frame(imtp->accepted_con, &frame);
    when_failed_trace(retval, exit, WARNING, "Read fd returned with %d", retval);

    mqtt_imtp_unpack_and_publish(imtp->client, frame);

exit:
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error reading data from socket, removing accepted connection");
        amxo_connection_remove(mqtt_get_parser(), fd);
        imtp_connection_delete(&imtp->accepted_con);
        imtp->accepted_con = NULL;
    }
    imtp_frame_delete(&frame);
    return;
}

static void mqtt_imtp_build_frame_pbuf(imtp_frame_t** frame,
                                       char* topic,
                                       char* pattern,
                                       void* payload,
                                       int payloadlen) {
    imtp_tlv_t* tlv_protobuf = NULL;

    imtp_frame_new(frame);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes, payloadlen, payload, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(*frame, tlv_protobuf);
    if(topic != NULL) {
        imtp_tlv_t* tlv_topic = NULL;
        imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, strlen(topic), topic, 0, IMTP_TLV_TAKE);
        imtp_frame_tlv_add(*frame, tlv_topic);
    }
    if(pattern != NULL) {
        imtp_tlv_t* tlv_pattern = NULL;
        imtp_tlv_new(&tlv_pattern, imtp_tlv_type_pattern, strlen(pattern), pattern, 0, IMTP_TLV_COPY);
        imtp_frame_tlv_add(*frame, tlv_pattern);
    }
}

static bool mqtt_imtp_needs_message(const char* interested_topics, const char* published_topic) {
    bool needs_message = true;
    amxc_string_t topics;
    amxc_llist_t list_of_topics;

    amxc_string_init(&topics, 0);
    amxc_llist_init(&list_of_topics);

    // When service did not provide a topic it is interested in, always return true
    when_str_empty(interested_topics, exit);
    when_str_empty(published_topic, exit);

    amxc_string_setf(&topics, "%s", interested_topics);
    amxc_string_split_to_llist(&topics, &list_of_topics, ',');

    amxc_llist_iterate(it, &list_of_topics) {
        amxc_string_t* topic = amxc_llist_it_get_data(it, amxc_string_t, it);
        SAH_TRACEZ_INFO(ME, "Message was published on %s and service is interested in %s", published_topic, amxc_string_get(topic, 0));
        if(strcmp(amxc_string_get(topic, 0), published_topic) == 0) {
            goto exit;
        }
    }

    needs_message = false;

exit:
    amxc_llist_clean(&list_of_topics, amxc_string_list_it_free);
    amxc_string_clean(&topics);
    return needs_message;
}

static void mqtt_imtp_connection_clean_up(amxc_llist_it_t* it) {
    mqtt_imtp_t* imtp_con = (mqtt_imtp_t*) amxc_llist_it_get_data(it, mqtt_imtp_t, it);

    if(imtp_con->listen_con != NULL) {
        amxo_connection_remove(mqtt_get_parser(), imtp_connection_get_fd(imtp_con->listen_con));
        imtp_connection_delete(&imtp_con->listen_con);
    }
    if(imtp_con->accepted_con != NULL) {
        amxo_connection_remove(mqtt_get_parser(), imtp_connection_get_fd(imtp_con->listen_con));
        imtp_connection_delete(&imtp_con->accepted_con);
    }
    free(imtp_con->topics);
    free(imtp_con);
}

void mqtt_imtp_connection_accept(int fd, void* priv) {
    mqtt_imtp_t* imtp = (mqtt_imtp_t*) priv;
    int fd_accept = -1;

    fd_accept = imtp_connection_accept(imtp->listen_con);
    if(fd_accept < 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to accept incoming connection, retval = [%d]", fd_accept);
        goto exit;
    }

    imtp->accepted_con = imtp_connection_get_con(fd_accept);
    when_null(imtp->accepted_con, exit);

    // Send handshake on accepted socket
    mqtt_imtp_send_handshake(imtp->accepted_con);

    // Stop listening for other connections
    amxo_connection_remove(mqtt_get_parser(), fd);
    imtp_connection_delete(&imtp->listen_con);
    imtp->listen_con = NULL;

    // Add accepted connection to event loop
    amxo_connection_add(mqtt_get_parser(),
                        imtp_connection_get_fd(imtp->accepted_con),
                        mqtt_imtp_accepted_read,
                        NULL,
                        (amxo_con_type_t) CON_ACCEPTED,
                        imtp);

    SAH_TRACEZ_INFO(ME, "Accepted incoming connection on listen socket with fd: [%d]",
                    imtp_connection_get_fd(imtp->accepted_con));

exit:
    return;
}

static char* mqtt_imtp_get_matching_pattern(amxd_object_t* object, const char* topic) {
    amxd_object_t* subscriptions = amxd_object_get(object, "Subscription");
    amxd_object_t* sub = NULL;
    mqtt_sub_t* sub_ref = NULL;
    bool match = false;

    amxd_object_for_each(instance, it, subscriptions) {
        sub = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(sub->priv == NULL) {
            sub = NULL;
            continue;
        }
        sub_ref = (mqtt_sub_t*) sub->priv;

        mqtt_topic_matches_sub(sub_ref->topic, topic, &match);
        if(match) {
            return sub_ref->topic;
        }
    }

    return NULL;
}

int mqtt_imtp_forward_message(amxd_object_t* obj, mqtt_imtp_t* imtp, mqtt_item_t* item) {
    int retval = -1;
    void* bmsg = NULL;
    imtp_frame_t* frame = NULL;
    amxc_var_t* props = GET_ARG(&item->data, ITEMF_PROPS);
    char* resp_topic = amxc_var_dyncast(cstring_t, GET_ARG(props, "response-topic"));
    const char* topic = GET_CHAR(&item->data, "topic");
    char* pattern = mqtt_imtp_get_matching_pattern(obj, topic);

    when_null(imtp->accepted_con, exit);
    // Check if service is interested in message published on this topic
    when_false(mqtt_imtp_needs_message(imtp->topics, topic), exit);

    if((resp_topic == NULL) && (topic != NULL)) {
        // Dirty fix -> needs to be updated with NET-4309
        resp_topic = strdup(topic);
    }

    SAH_TRACEZ_INFO(ME, "Sending message to agent with response-topic %s pattern %s", resp_topic, pattern);
    mqtt_imtp_build_frame_pbuf(&frame, resp_topic, pattern, item->payload, item->payloadlen);
    retval = imtp_connection_write_frame(imtp->accepted_con, frame);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending tlv over uds");
    }

exit:
    free(bmsg);
    imtp_frame_delete(&frame);
    return retval;
}

void mqtt_imtp_connections_clean(mqtt_client_t* client) {
    when_null(client, exit);
    amxc_llist_clean(&client->imtp_cons, mqtt_imtp_connection_clean_up);
exit:
    return;
}
