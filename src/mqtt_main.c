/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "dm_mqtt_utilities.h"
#include "mqtt_interface.h"
#include "mqtt_imtp.h"
#include "mqtt_usp.h"
#include "mqtt_dns.h"

static mqtt_app_t app;

// Debugging - print events - activate in odl
void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    printf("event received - %s\n", sig_name);
    if(data != NULL) {
        printf("Event data = \n");
        fflush(stdout);
        amxc_var_dump(data, STDOUT_FILENO);
    }
}

static void mqtt_create_socket_dir(void) {
    if(amxp_dir_make(SOCKET_DIR, 0700) != 0) {
        SAH_TRACEZ_INFO(ME, "Failed to create directory %s. Make sure you have the right permissions", SOCKET_DIR);
    } else {
        SAH_TRACEZ_INFO(ME, "Successfully created directory %s", SOCKET_DIR);
    }
}

static void mqtt_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "**************************************");
    SAH_TRACEZ_INFO(ME, "*            MQTT started            *");
    SAH_TRACEZ_INFO(ME, "**************************************");
    app.dm = dm;
    app.parser = parser;
    app.msg_id = 0;
    netmodel_initialize();
    mqtt_init_lib();
    mqtt_dns_lib_init();
    mqtt_create_socket_dir();
    mqtt_usp_setup_listen_socket(parser);
    mqtt_set_mqtt_event_handlers();
    amxo_connection_add(parser,
                        mqtt_ctrl_fd(),
                        mqtt_ctrl_handle_item,
                        NULL,
                        AMXO_CUSTOM,
                        NULL);
    amxp_sigmngr_add_signal(NULL, "mqtt:disconnected");
}

static void mqtt_exit(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    mqtt_dns_lib_clean();
    mqtt_cleanup_lib();
    netmodel_cleanup();
    app.dm = NULL;
    app.parser = NULL;
}

amxd_dm_t* PRIVATE mqtt_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE mqtt_get_parser(void) {
    return app.parser;
}

int32_t PRIVATE mqtt_next_msg_id(void) {
    return app.msg_id++;
}

int _mqtt_main(int reason,
               amxd_dm_t* dm,
               amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "mqtt_main, reason: %d", reason);
    switch(reason) {
    case 0:     // START
        mqtt_init(dm, parser);
        break;
    case 1:     // STOP
        mqtt_exit(dm, parser);
        break;
    default:
        break;
    }

    return 0;
}
