/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ares.h>

#include "mqtt_interface.h"
#include "mqtt_dns.h"
#include "dm_mqtt_props.h"
#include "dm_mqtt_utilities.h"
#include "dm_mqtt.h"

static void mqtt_dns_resolved_cb(void* arg, int status, UNUSED int timeouts, struct hostent* host) {
    int retval = -1;
    amxd_param_t* passwd_param = NULL;
    const char* passwd = NULL;
    char* ip_addr = NULL;
    amxd_object_t* object = (amxd_object_t*) arg;
    mqtt_client_t* client = NULL;
    amxc_var_t props;
    amxc_var_t params;

    amxc_var_init(&props);
    amxc_var_init(&params);

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);

    // fd is no longer valid and must be removed from the event loop, even in case of failure (HGWKPN-2335)
    amxo_connection_remove(mqtt_get_parser(), client->dns_query.socket_fd);
    client->dns_query.socket_fd = 0;

    when_true_trace(status != ARES_SUCCESS, exit, ERROR, "Failed to resolve IP for client: %s",
                    amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    when_null(host, exit);
    when_true(host->h_length < 1, exit);

    // success, so stop retransmissions
    amxp_timer_stop(client->dns_query.retransmission);

    ip_addr = inet_ntoa(*(struct in_addr*) host->h_addr_list[0]);
    SAH_TRACEZ_INFO(ME, "Resolved %s to %s", host->h_name, ip_addr);

    dm_mqtt_props_connect(&props, object);
    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    passwd_param = amxd_object_get_param_def(object, "Password");
    passwd = amxc_var_constcast(cstring_t, &passwd_param->value);

    mqtt_client_update_status(object, "Connecting");
    if(strcmp(GET_CHAR(&params, "TransportProtocol"), "TLS") == 0) {
        SAH_TRACEZ_INFO(ME, "Connecting with hostname for TLS certificate checking");
        retval = mqtt_connect(client->con, GET_CHAR(&params, "BrokerAddress"), GET_UINT32(&params, "BrokerPort"),
                              GET_UINT32(&params, "KeepAliveTime"), GET_CHAR(&params, "Username"),
                              passwd, GET_CHAR(&params, "ProtocolVersion"),
                              GET_UINT32(&params, "TcpSendMem"), &props);
    } else {
        retval = mqtt_connect(client->con, ip_addr, GET_UINT32(&params, "BrokerPort"),
                              GET_UINT32(&params, "KeepAliveTime"), GET_CHAR(&params, "Username"),
                              passwd, GET_CHAR(&params, "ProtocolVersion"),
                              GET_UINT32(&params, "TcpSendMem"), &props);
    }
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error during mqtt_connect");
        mqtt_client_update_status(object, "Error");
        if(client->reconnect_busy) {
            mqtt_client_force_reconnect_failed(client);
        }
        mqtt_delete(&(client)->con);
    }

exit:
    amxc_var_clean(&props);
    amxc_var_clean(&params);
}

static void handle_ares_event_cb(int fd, void* priv) {
    ares_channel channel = (ares_channel) priv;
    ares_process_fd(channel, fd, ARES_SOCKET_BAD);
}

static void mqtt_ares_sock_cb(void* data, ares_socket_t socket_fd, int readable, UNUSED int writable) {
    amxo_parser_t* parser = mqtt_get_parser();
    dns_query_t* query = (dns_query_t*) data;

    SAH_TRACEZ_INFO(ME, "ares_init_options cb function with fd: %d", socket_fd);
    if((readable > 0) && (amxo_connection_get(parser, socket_fd) == NULL)) {
        SAH_TRACEZ_INFO(ME, "Add fd %d to amxrt event loop", socket_fd);
        amxo_connection_add(parser, socket_fd, handle_ares_event_cb, NULL, AMXO_CUSTOM, query->channel);
        query->socket_fd = socket_fd;
    }
}

static void mqtt_resolve_stop(dns_query_t* query) {
    if(query->socket_fd > 0) {
        amxo_connection_remove(mqtt_get_parser(), query->socket_fd);
        query->socket_fd = 0;
    }
    if(query->channel != NULL) {
        ares_destroy(query->channel);
        query->channel = NULL;
    }
}

static int mqtt_resolve_start(amxd_object_t* object, dns_query_t* query) {
    int retval = -1;
    uint32_t timeout = amxc_var_dyncast(uint32_t, query->interval);
    char* broker_address = amxd_object_get_value(cstring_t, object, "BrokerAddress", NULL);

    mqtt_client_update_status(object, "Connecting");

    SAH_TRACEZ_INFO(ME, "Start resolving address %s", broker_address);
    retval = ares_init_options(&query->channel, &query->options, query->optmask);
    if(retval != ARES_SUCCESS) {
        SAH_TRACEZ_ERROR(ME, "Failed to create c-ares channel: %s", ares_strerror(retval));
        goto exit;
    }

    if(timeout > 0) {
        SAH_TRACEZ_INFO(ME, "Start timer[retransmission] %ums", timeout);
        amxp_timer_start(query->retransmission, timeout);
    }
    ares_gethostbyname(query->channel, broker_address, AF_INET, mqtt_dns_resolved_cb, object);

exit:
    free(broker_address);
    return retval;
}

static void mqtt_dns_retry_cb(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* object = (amxd_object_t*) priv;
    mqtt_client_t* client = (mqtt_client_t*) object->priv;
    char* broker_address = amxd_object_get_value(cstring_t, object, "BrokerAddress", NULL);
    amxc_var_t* next_timeout = NULL;

    mqtt_client_update_status(object, "Error_BrokerUnreachable");

    SAH_TRACEZ_INFO(ME, "Timer[retransmission] expired");
    when_null(client, exit);

    mqtt_resolve_stop(&client->dns_query);

    next_timeout = amxc_var_get_next(client->dns_query.interval);
    if(next_timeout != NULL) {
        client->dns_query.interval = next_timeout;
    }
    when_null_trace(client->dns_query.interval, exit, ERROR, "Stop DNS lookup");

    mqtt_resolve_start(object, &client->dns_query);

exit:
    free(broker_address);
}

static int mqtt_dns_init(amxd_object_t* object, dns_query_t* query) {
    int retval = -1;

    amxc_var_init(&query->intervals);

    if(amxd_object_get_param(object, "RetransmissionStrategy", &query->intervals) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to get RetransmissionStrategy");
        amxc_var_clean(&query->intervals);
    } else {
        amxc_var_cast(&query->intervals, AMXC_VAR_ID_LIST);

        query->interval = amxc_var_get_first(&query->intervals);
        if(query->interval == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not get first timeout from RetransmissionStrategy");
            amxc_var_clean(&query->intervals);
        }
    }

    if(query->retransmission == NULL) {
        amxp_timer_new(&query->retransmission, mqtt_dns_retry_cb, object);
    }

    query->options.sock_state_cb = mqtt_ares_sock_cb;
    query->options.sock_state_cb_data = query;
    query->options.tries = 1;
    query->options.timeout = 300 * 1000; // 5 minutes
    query->optmask = ARES_OPT_SOCK_STATE_CB | ARES_OPT_TIMEOUTMS | ARES_OPT_TRIES;

    retval = mqtt_resolve_start(object, query);

    return retval;
}

void mqtt_dns_clean(dns_query_t* query, bool delete_timer) {
    mqtt_resolve_stop(query);
    if(amxc_var_type_of(&query->intervals) != AMXC_VAR_ID_NULL) {
        amxc_var_clean(&query->intervals);
    }
    if(delete_timer) {
        amxp_timer_delete(&query->retransmission);
    } else {
        amxp_timer_stop(query->retransmission);
    }
}

int mqtt_dns_resolve(amxd_object_t* object) {
    mqtt_client_t* client = (mqtt_client_t*) object->priv;

    mqtt_dns_clean(&client->dns_query, false);

    return mqtt_dns_init(object, &client->dns_query);
}

int mqtt_dns_lib_init(void) {
    int retval = ares_library_init(ARES_LIB_INIT_ALL);
    if(retval != ARES_SUCCESS) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize c-ares library: %s\n", ares_strerror(retval));
    }

    return retval;
}

void mqtt_dns_lib_clean(void) {
    ares_library_cleanup();
}
